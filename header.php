<?php
if (!isset($_SESSION))
    session_start();
include('utils.php');
?>
<!DOCTYPE html>
<html lang="it">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Gestore Eventi</title>
    <!-- Favicon -->
    <link href="assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/custom/custom.css" type="text/css">
    <link rel="stylesheet" href="css/stile.css" type="text/css">
    <!-- Argon CSS -->  
    <link type="text/css" href="assets/css/argon.css?v=1.1.0" rel="stylesheet">
</head>

<body>
    <header class="header-global">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <div class="container">
                <?php if (!isset($_SESSION["MANAGER"])) { ?>
                    <a class="navbar-brand" href="./index">
                        <h3 style="color:white" class="display-4">Eventi</h3>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar-default">
                        <div class="navbar-collapse-header">
                            <div class="row">
                                <div class="col-6 collapse-brand">
                                    <h3>Eventi</h3>
                                </div>
                                <div class="col-6 collapse-close">
                                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                                        <span></span>
                                        <span></span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                            <li class="nav-item dropdown">
                                <a href="index" class="nav-link" role="button">
                                    <i class="ni ni-shop d-lg-none"></i>
                                    <span class="nav-link-inner--text">Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="listaEventi" class="nav-link" role="button">
                                    <i class="ni ni-collection d-lg-none"></i>
                                    <span class="nav-link-inner--text">Tutti gli Eventi</span>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link" data-toggle="dropdown" role="button" aria-expanded="true">
                                    <i class="ni ni-bullet-list-67 d-lg-none"></i>
                                    <span class="nav-link-inner--text">Categorie</span>
                                </a>
                                <div class="dropdown-menu">
                                    <?php
                                    $rows = $dbh->getCategorie();
                                    foreach ($rows as $row) {
                                    ?>
                                        <a class="dropdown-item" href=<?php echo "categorie?categoria=" . ucfirst($row) ?>> <?php echo $row ?></a>
                                    <?php } ?>
                                </div>
                            </li>
                        </ul>

                        <ul class="navbar-nav ml-lg-auto">
                            <?php
                            if (isset($_COOKIE["LOGIN"]) || isset($_SESSION["LOGIN"])) {
                                echo '<li class="nav-item dropdown">
                            <a class="nav-link nav-link-icon notifiche" href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="badge badge-danger count"></span>
                                <i class="ni ni-bell-55"></i>
                                <span class="nav-link-inner--text d-lg-none">Notifiche</span>
                            </a>
                            <div class="dropdown-menu dropNotifiche" aria-labelledby="navbar-default_dropdown_1">
                            </div>

                            </li>';
                            } ?>

                            <li class="nav-item dropdown">
                                <a class="nav-link nav-link-icon" href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ni ni-circle-08"></i>
                                    <span class="nav-link-inner--text d-lg-none">Account</span>
                                </a>
                                <?php
                                if (isset($_COOKIE["LOGIN"]) || isset($_SESSION["LOGIN"])) {
                                    echo '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                                <a class="dropdown-item" href="myprofile">Il mio Profilo</a>';
                                    if (isset($_SESSION["MANAGER"])) {
                                        echo '<a class="dropdown-item" href="eventiOrganizzati">Lista Eventi</a>';
                                    } else {
                                        echo '<a class="dropdown-item" href="ordini">I miei Ordini</a>';
                                    }
                                    echo '<div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="php/logout">Esci</a>
                            </div>';
                                } else {
                                    echo '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                            <a class="dropdown-item" href="login">Accedi</a>';
                                    if (isset($_SESSION["MANAGER"])) {
                                        echo '<a class="dropdown-item" href="user">Vista utente</a>';
                                    } else {
                                        echo '<a class="dropdown-item" href="manager">Sei un Organizzatore?</a>';
                                    }
                                    echo '
                            <a class="dropdown-item" href="login">Il mio Profilo</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="signupView">Registrati</a>
                        </div>';
                                }

                                ?>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-link-icon" href="carrello">
                                    <i class="ni ni-cart"></i>
                                    <span class="nav-link-inner--text d-lg-none">Carrello</span>
                                </a>
                            </li>
                        </ul>

                    </div>
            </div>
        <?php } else { ?>
            <a class="navbar-brand" href="eventiOrganizzati">
                <h3 style="color:white" class="display-4">Eventi</h3>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-default">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <h3>Eventi</h3>
                        </div>
                        <div class="col-6 collapse-close">
                            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>

                <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                    <li class="nav-item dropdown">
                        <a href="eventiOrganizzati" class="nav-link" role="button">
                            <i class="ni ni-shop d-lg-none"></i>
                            <span class="nav-link-inner--text">Eventi Organizzati</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="aggiungiEvento" class="nav-link" role="button">
                            <i class="ni ni-shop d-lg-none"></i>
                            <span class="nav-link-inner--text">Aggiungi un Evento</span>
                        </a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-lg-auto">
                    <?php
                    if (isset($_COOKIE["LOGIN"]) || isset($_SESSION["LOGIN"])) {
                        echo '<li class="nav-item dropdown">
                            <a class="nav-link nav-link-icon notifiche" href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="badge badge-danger count"></span>
                                <i class="ni ni-bell-55"></i>
                                <span class="nav-link-inner--text d-lg-none">Notifiche</span>
                            </a>
                            <div class="dropdown-menu dropNotifiche" aria-labelledby="navbar-default_dropdown_1">
                            </div>

                            </li>';
                    } ?>

                    <li class="nav-item dropdown">
                        <a class="nav-link nav-link-icon" href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ni ni-circle-08"></i>
                            <span class="nav-link-inner--text d-lg-none">Account</span>
                        </a>
                        <?php
                        if (isset($_COOKIE["LOGIN"]) || isset($_SESSION["LOGIN"])) {
                            echo '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                                <a class="dropdown-item" href="myprofile">Il mio Profilo</a>';
                            echo '<a class="dropdown-item" href="eventiOrganizzati">Lista Eventi</a>';
                            echo '<div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="php/logout">Esci</a>
                            </div>';
                        } else {
                            echo '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                            <a class="dropdown-item" href="login">Accedi</a>';
                            echo '<a class="dropdown-item" href="user">Sei un Utente?</a>';
                            echo '
                            <a class="dropdown-item" href="login">Il mio Profilo</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="signupView">Registrati</a>
                        </div>';
                        }

                        ?>
                    </li>
                </ul>

            </div>
            </div>
        <?php } ?>

        </nav>
    </header>