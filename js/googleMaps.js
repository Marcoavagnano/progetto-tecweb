// Initialize and add the map
function initMap() {
    var latitude = 44.030700;
    var longitude = 12.614910;
    var ADDRESS = document.getElementById("address").textContent;
    var client = new HttpClient();
    client.get('https://us1.locationiq.com/v1/search.php?key=744952e244952f&q='+ ADDRESS + '&format=json'
    , function(response) {
        latitude = (JSON.parse(response))[1].lat;
        longitude = (JSON.parse(response))[1].lon;
        console.log(longitude);
        console.log(latitude);
        console.log(response);

        latitude = parseFloat(latitude);
        longitude= parseFloat(longitude);
         // The location of Uluru
    var uluru = {
        lat: latitude,
        lng: longitude
    };
    // The map, centered at Uluru
    var map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 10,
            center: uluru
        });
    // The marker, positioned at Uluru
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
});  
}

var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() { 
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }

        anHttpRequest.open( "GET", aUrl, true );            
        anHttpRequest.send( null );
    }
}
