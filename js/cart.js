$(document).ready(function() {
    var sum = 0;
    var subtotal = 0;
    var shippingRange = 50;
    init();
    function init(){
        getSum();
        updateSum();
    }
    function getSum(){
    subtotal=0;
    if($( '.cart-row' ).length) {
        $('.cart-row').each(function() {
            var $this = $(this); //get initial subtotal
            let group = jQuery(this).attr("id").substring(3);
            let quantity=Number($this.find("div.itemCounter"+group).text());
            subtotal += Number($this.find(".price").text().substring(1))*quantity;
        });
    }
}
    $(".inc").on("click", function() {
        let group = jQuery(this).attr("id");
        let id = $(this).attr("name");
        let quantity = parseInt($(".itemCounter" + group).text());
        quantity++;
        $(".itemCounter" + group).text(quantity);
        $(".m-itemCounter" + group).text(quantity);
     
        let cookie = getCookie("cart");
     
          for (let i = 0; i < cookie.length; i++) {
            if (cookie[i].item_id == id) {
                cookie[i].item_quantity=quantity;
            }
          }
        cookie = JSON.stringify(cookie);
        
        updateCookie(cookie, "cart");
        modifySubtotal(group, "+");

    });
    $(".dec").on("click", function() {
        let group = jQuery(this).attr("id");
        let id = $(this).attr("name");
        let quantity = parseInt($(".itemCounter" + group).text());
        if (quantity > 1) {
            quantity--;
            $(".itemCounter" + group).text(quantity);
            $(".m-itemCounter" + group).text(quantity);

        let cookie = getCookie("cart");
    
        for (let i = 0; i < cookie.length; i++) {
            if (cookie[i].item_id == id) {
                cookie[i].item_quantity=quantity;
            }
        }
          cookie = JSON.stringify(cookie);
          updateCookie(cookie, "cart");
          modifySubtotal(group, "-");
        }
    });
    $(".rm-line").on("click", function() {
        let rowInCart=$( '.cart-row' ).length;
        
        let group = $(this).attr("id");
        $('.rm-line').each(function() { //elimino sia versione desktop che mobile
            $("#row" + group).remove();
        });
        if(rowInCart==2){ //se sono rimaste solo le due righe del desktop e mobile
            $('.m-list-item').remove();
        } 
        let id = $(this).attr("name");
        removeIdCookie(id);
        getSum();
        updateSum();
        console.log("Eco");
    });
    $("#pagamentobtn").on("click", function() {
        $.post("conferma_ordine.php");
    });


    function modifySubtotal(group, mode) {
        let price = Number($('#row' + group).find('.price').text().substring(1));
        if (mode == '+') {
            subtotal += price;
        }
        if (mode == '-') {
            subtotal -= price;
        }
        updateSum();
    }

    function updateSum() {
        let shippingFees = 0;
        $('.partial-price').text("€" + subtotal);
        if (subtotal < shippingRange)
            shippingFees = parseInt((subtotal * 15) / 100); //calculate 10% of fees
        sum = subtotal + shippingFees;
        $('.shipping-fees').text("€" + shippingFees);
        $('.total-price').text('€' + sum);
    }
    
    function removeIdCookie(id) {
        let cookie = getCookie("cart");
        const index = cookie.findIndex(x => x.item_id === id);
        if (index !== undefined) cookie.splice(index, 1);
        cookie = JSON.stringify(cookie);
        updateCookie(cookie, "cart");
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return JSON.parse(c.substring(name.length, c.length));
            }
        }
        return "";
    }

    function updateCookie(ccontent, cname) {
        let date = new Date();
        date.setTime(date.getTime() + (10 * 60 * 1000));
        let expires = "; expires=" + date.toGMTString();
        document.cookie = cname + "=" + ccontent + expires + "; path=/";
    }
});