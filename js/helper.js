function truncateText(selector, maxLength) {
    var element = selector,
        truncated = element.innerText;

    if (truncated.length > maxLength) {
        truncated = truncated.substr(0, maxLength) + '...';
    }
    return truncated;
}

var bothElements = document.querySelectorAll("[id='descrizione_evento']");

for (let i = 0; i < bothElements.length; i++) {
    console.log(bothElements[i]);
    bothElements[i].innerText = truncateText(bothElements[i], 70);
}