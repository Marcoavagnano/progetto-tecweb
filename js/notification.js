$(function() {
    // updating the view with notifications using ajax
    function load_unseen_notification(view = '') {
        $.ajax({
            url: "php/notifiche.php",
            method: "POST",
            data: {
                view: view
            },
            dataType: "json",
            success: function(data) {
                console.log(data);
                if (data.count > 0) {
                    $('.count').html(data.count);
                }
                $('.dropNotifiche').html(data.notification);
            },
            error: function(error) {
                console.log(error)
            }
        });
    }
    load_unseen_notification();
    // load new notifications
    $(document).on('click', '.notifiche', function() {
        $('.count').html('');
        load_unseen_notification('yes');
    });
    setInterval(function() {
        load_unseen_notification();
    }, 5000);
});