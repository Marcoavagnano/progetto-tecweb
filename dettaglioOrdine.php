<?php
session_start();
if (isset($_SESSION['LOGIN'])) :
    require("utils.php");
    include('header.php');
    include('utils.php');
?>
    <section class="section">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <h1>Dettaglio Ordine</h1>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered cellspacing=" 0" width="100%">
                    <caption>Dettaglio Ordine</caption>
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Data Inizio</th>
                            <th scope="col">Data Fine</th>
                            <th scope="col">Prezzo</th>
                            <th scope="col">Quantità</th>
                        </tr>
                    </thead>
                    <?php
                    $id_ordine = $_POST['edit_id'];
                    foreach ($dbh->getDettaglioOrdineByIdOrdine($id_ordine) as $ordine) {
                        $evento = $dbh->getEventoById($ordine['id_evento']);
                    ?>
                        <tr>
                            <td>
                                <a href="dettaglioEvento?edit_id=<?php echo $evento['id']; ?> ">
                                    <img src=" <?php echo $dbh->getImgSrc($evento['foto']) ?>" alt="Immagine Evento" class="img-fluid rounded shadow" style="width: 300px;">
                                </a>
                            </td>
                            <td>
                                <?php echo $evento['nome']; ?>
                            </td>
                            <td>
                                <?php echo  date('d-m-Y', strtotime($evento['data_inizio'])) ?>
                            </td>
                            <td>
                                <?php echo  date('d-m-Y', strtotime($evento['data_fine'])); ?>
                            </td>
                            <td>
                                <?php echo $evento['prezzo'] . "€"; ?>
                            </td>
                            <td>
                                <?php echo $ordine['quantita']; ?>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </section>
<?php
    include('footer.php');
else :
    // Redirect them to the login page
    header('Location: login.php');
    die();
endif;
?>

<!-- <div class="row">
                        <div class="col col-md-3">

                        </div>
                    </div> -->