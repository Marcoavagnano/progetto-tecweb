<?php
session_start();
include('utils.php');
include('header.php');
if (isset($_GET["categoria"])) :
  $categoria = $_GET["categoria"];
?>
  <div class="container ">
    <div class="jumbotron">
      <h1><?php echo $categoria ?></h1>
    </div>

    <!-- Example row of columns -->
    <div class="row">
      <?php
      foreach ($dbh->getEventiByCategoria($dbh->getIdByNomeCategoria($categoria)) as $row) {   ?>
        <div class="col-lg-4">

          <img src="<?php echo $dbh->getImgSrc($row['foto']); ?>" class="img-thumbnail" alt="Foto Evento">
          <h2><?php echo $row['nome'] ?></h2>
          <p id="descrizione_evento"><?php echo $row['descrizione'] ?></p>
          <form action="dettaglioEvento.php" method="GET">
            <input type='hidden' name='id_evento' value='<?php echo $row['id'] ?>'>
            <p><button class="btn btn-primary">Vai all' Evento &raquo;</button></p>
          </form>
        </div>
      <?php } ?>
    </div>

  </div>
<?php
  include('footer.php');
else :
  echo "Errore! 404";
endif;
?>