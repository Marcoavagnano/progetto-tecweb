<?php
session_start();
include('utils.php');
include('header.php');
$id = $_GET['id_evento'];

$result = $dbh->getEventoById($id);
?>
<section class="section">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index">Home</a></li>
                <li class="breadcrumb-item" aria-current="page"><a href="listaEventi">Lista Eventi</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#"><?php echo $result['nome'] ?></a></li>
            </ol>
        </nav>
        <img src="<?php echo $dbh->getImgSrc($result['foto']); ?>" class="img-fluid" alt="Responsive image">
        <div class="py-5 bg-secondary">
            <div class="row justify-content-center" style="margin-top: 15px; margin-bottom: 10px;">
                <div class="col-md-4 col-sm-4" id="col_dettaglio">
                    <div class="info">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span>
                            Orario:
                        </span>
                    </div>
                    <div><?php echo date("H:i", strtotime($result['ora'])); ?></div>
                </div>
                <div class="col-md-4 col-sm-4" id="col_dettaglio">
                    <div class="info">
                        <i class="ni ni-calendar-grid-58" aria-hidden="true"></i>
                        <span>Data:</span>
                    </div>
                    <div><?php echo "Dal " .  date('d-m-Y', strtotime($result['data_inizio']));
                            echo " ";
                            echo " al " . date('d-m-Y', strtotime($result['data_fine']));  ?></div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-4 col-sm-4" id="col_dettaglio">
                    <div class="info">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        <span>Luogo:</span>
                    </div>
                    <div id="address"><?php foreach ($dbh->getIndirizzoCitta($result['luogo']) as $row) {
                                            echo $row['indirizzo'] . ", ";
                                            echo $row['citta'];
                                        }; ?></div>
                </div>
                <div class="col-md-4 col-sm-4" id="col_dettaglio">
                    <div class="info">
                        <i class="ni ni-money-coins" aria-hidden="true"></i>
                        <span>Prezzo:</span>
                    </div>
                    <div><?php echo $result['prezzo'] . " €"; ?></div>
                </div>
            </div>
        </div>
        <div class="text-center mt-5">
            <h2><?php echo $result['nome'] ?></h2>
        </div>
        <div class="mt-5 py-5 border-top text-center">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <p><?php echo $result['descrizione'] ?></p>
                    <iframe name="votar" style="display:none;"></iframe>
                    <form action="php/aggiungiAlCarrello.php" method="post" target="votar">
                        <input type="hidden" name="cart_id" value="<?php echo $id ?> ">
                        <button data-toggle="modal" data-target="#carrelloModal" type="submit" class="btn btn-primary">Aggiungi al carrello</button>
                    </form>
                </div>
            </div>
        </div>
        <div id="map">

        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="carrelloModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Evento aggiunto al carrello.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok!</button>
                <a href="carrello">
                    <button type="button" class="btn btn-primary">Vai al carrello!</button>
                </a>
            </div>
        </div>
    </div>
</div>
<?php
include('footer.php');
?>