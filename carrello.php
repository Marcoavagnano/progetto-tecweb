<?php
if (!isset($_SESSION))
  session_start();
include('utils.php');
include('header.php');


if (isset($_SESSION['empty_cart'])) {
  echo   '<div class="alert alert-danger m-2 text-center" role="alert">';
  echo $_SESSION['empty_cart'];
  echo "</div>";
  unset($_SESSION['empty_cart']);
}
    ?>
<section class="section">

  <div class="container" id="desktopEventiOrg">
  
    <div class="row">

      <div class="col-lg-6 bg-white">

        <!-- Shopping cart table -->

        <div class="table-responsive ">

          <table class="table " style="border-radius: 10px 0 0 0;">
            <thead>
              <tr>
                <th scope="col" class="border-0">
                  <div class="p-2 px-3 text-uppercase">Evento</div>
                </th>
                <th scope="col" class="border-0 ">
                  <div class="py-2 text-uppercase">Prezzo</div>
                </th>
                <th scope="col" class="border-0">
                  <div class="py-2 text-uppercase">Quantità</div>
                </th>
                <th scope="col" class="border-0 ">
                  <div class="py-2 text-uppercase">Rimuovi</div>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              $rowCounter = 0;
              $btnCounter = 0;
              if (isset($_COOKIE["cart"])) :
                $arrayEventi = json_decode($_COOKIE["cart"], true);
                foreach ($arrayEventi as $keys) :
                  $row = $dbh->getEventoById($keys["item_id"]);
                  echo '
            <tr id="row' . $rowCounter . '" class="cart-row">
              <th scope="row" class="border-0">
                <div class="p-2">
                  <img src="' . $dbh->getImgSrc($row['foto']) . '" alt="" width="70" class="img-fluid rounded shadow-sm">
                  <div class=" mt-2 d-inline-block">
                    <h5 class="mb-0">' . $row['nome'] . '</h5><span class="ml-1 text-muted font-weight-normal font-italic d-block">Categoria : ' . $dbh->getCategoriaById($row['categoria']) . '</span>
                  </div>
                </div>
              </th>
              <td class="align-middle border-0"><strong class="price">€' . $row['prezzo'] . '</strong></td>


              <td class="align-middle align-center border-0">
              <div class="input-group-prepend">
                  <button class="btn btn-secondary btn-outline-primary dec" style="padding: 10px;" id="' . $btnCounter . '"  name="' . $row['id'] . '">-</button>
                  <div class="itemCounter' . $rowCounter . ' p-2"> ' . $keys["item_quantity"] .  ' </div>
                  <button class="btn btn-secondary btn-outline-primary ml-2 p-2 inc" id="' . $btnCounter . '"  name="' . $row['id'] . '">+</button>
               </div>
             
              </td>
              <td class="align-middle border-0"><h5 ><a name="' . $row['id'] . '" class="text-dark rm-line" id="' . $rowCounter . '"><i class="fa fa-trash ml-4 pt-3"></i></a></h5></td>
            </tr>';
                  $btnCounter++;
                  $rowCounter++;
                endforeach;
              endif;
              ?>

            </tbody>
          </table>
        </div>

        <!-- End -->

      </div>
      <div class="col-lg-6">
        <div class="rounded-pill px-4 align-center py-3 text-uppercase "><strong>CHECKOUT</strong></div>
        <div class="p-4">
          <p class="font-italic mb-4">Le spese di spedizione sono calcolate in base al costo dell'ordine. Sono gratuite per ordini superiori a € 50 </p>
          <ul class="list-unstyled mb-4">
            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Parziale ordine </strong><strong class=" partial-price"></strong></li>
            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Spese di spedizione</strong><strong class="shipping-fees"></strong></li>

            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Totale</strong>
              <h5 class="font-weight-bold total-price"></h5>
            </li>
          </ul>
          <a href="confermaPagamento" class="btn btn-dark "> Esegui il checkout</a>

        </div>
      </div>
    </div>
  </div>
  </div>

<div class="container" id="mobileOrganizzati">

<!-- For demo purpose -->
<div class="row text-center text-white mb-5">
    <div class="col-lg-7 mx-auto">
        <h1 class="display-4">Eventi nel carrello</h1>
    </div>
</div>
<!-- End -->

<div class="row">
    <div class="col-lg-8 mx-auto">

        <!-- List group-->
        <ul class="list-group shadow">
            <?php
        
             $rowCounter = 0;
             $btnCounter = 0;
             if (isset($_COOKIE["cart"])) :
               $arrayEventi = json_decode($_COOKIE["cart"], true);
               foreach ($arrayEventi as $keys) :
                 $row = $dbh->getEventoById($keys["item_id"]);
            ?>
                <!-- list group item-->
                <li class="list-group-item m-list-item">

                    <div class="row cart-row"  id="row<?php echo $rowCounter ?>">
                    <div class="col col-3 ">
                    <h5> Evento</h5>
                <img src=" <?php echo $dbh->getImgSrc($row['foto']) ?>" alt="Immagine Evento" class="img-fluid rounded shadow mt-2" style="width: 50px;">
                    </div>
                        <div class="col col-3">
                       <h5> Quantità</h5>
                       <div class="input-group-prepend ">
                         <button class="btn btn-secondary btn-outline-primary dec" style="padding: 7px;"  id="<?php echo $btnCounter ?>"  name="<?php echo $row['id'] ?>">-</button>
                        <div class="m-itemCounter<?php echo $rowCounter ?> mt-2 mr-2"> <?php echo $keys['item_quantity'] ?> </div>
                         <button class="btn btn-secondary btn-outline-primary  inc" style="padding: 6px;" id="<?php echo $btnCounter ?>"  name="<?php echo $row['id'] ?>">+</button>
                        </div>
                        </div>
                    
                        <div class="col col-3 pl-5">
                        
                      <h5 >Prezzo</h5>
                      <div class="text-center mt-3 price">€<?php echo $row['prezzo']; ?></div>
                        </div>
                        <div class="col col-3 center-block " >
                       <div class="text-center" style="margin-top: 43px;">
                        <a name="<?php echo $row["id"] ?>" class="text-dark rm-line" id="<?php echo $rowCounter ?>"><i class="fa fa-trash"></i></a>
                        </div>
                        </div>
                    </div>
                </li>
            <?php
                $btnCounter++;
                $rowCounter++;
              endforeach;
            endif;
            ?>
           
            <!-- End -->
        </ul>
        <div class="col-lg-6 mt-5">
        <div class="rounded-pill  align-center p-1 "><h5 class="font-weight-bold">CHECKOUT</h5></div>
        <div class="p-2">
          <p class="font-italic mb-4">Le spese di spedizione sono calcolate in base al costo dell'ordine. Sono gratuite per ordini superiori a € 50 </p>
          <ul class="list-unstyled mb-4">
            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Parziale ordine </strong><strong class=" partial-price"></strong></li>
            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Spese di spedizione</strong><strong class="shipping-fees"></strong></li>

            <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Totale</strong>
              <h5 class="font-weight-bold total-price"></h5>
            </li>
          </ul>
          <a href="confermaPagamento" class="btn btn-dark "> Esegui il checkout</a>

        </div>
      </div>
        <!-- End -->
    </div>
</div>
</div>
    </section>

<?php
include('footer.php');
?>