<?php
    if (!isset($_SESSION))
        session_start();
    require("../utils.php");
    $manager = false;
    $name = $_POST["regname"];
    $surname = $_POST["regsurname"];
    $date = $_POST["regdate"];
    $email = $_POST["regemail"];
    $address = $_POST["regaddress"];
    $password = $_POST["regpass"];
    $cpassword = $_POST["regconfpass"];
    if (isset($_COOKIE["LOGIN"]))
        $idUtente = $_COOKIE["LOGIN"];
    if (isset($_SESSION["LOGIN"])) {
        $idUtente = $_SESSION["LOGIN"];
    }
    if(isset($_SESSION["MANAGER"])){
        $pIva=$_POST["regiva"];
        $manager=true;
    }

    if (($password ===  $cpassword)) {
        $passOk = true;
    } else {
        $passOk = false;
        $return = "Le password non coincidono";
        $_SESSION['errSignup'] = $return;
    }
   
    if (
        empty($name) || empty($surname) || empty($date) || empty($email) ||
        empty($address) || empty($password) || empty($cpassword)) {
        $return = "Riempi ogni campo correttamente";
        $_SESSION['errSignup'] = $return;
    }
    if (!isset($_SESSION['errSignup'])) {
        if(!$manager)
            $dbh->addUser($name, $surname, $date, $email, $address, $password);
        else
            if(!$dbh->addManager($name, $surname, $date,$pIva, $email, $address, $password)){
                $queryErr="La query non è andata a buon fine";
                $_SESSION['errSignup']=$queryErr;
            }
    } 
    if(!isset($_SESSION['errSignup']))
    {
        if (!isset($_SESSION["MANAGER"])) {
            $id=$dbh->getIdUserByEmail($email);
            $_SESSION["LOGIN"] = $id;
        }
        else{
             $id=$dbh->getIdManagerByEmail($email);
             $_SESSION["LOGIN"] = $id;
        }
        header("Location: ../index");
    }else{
        header("Location: ../signupView");
    }
