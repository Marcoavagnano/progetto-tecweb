<?php
include('../utility/utils.php');
session_start();
if (isset($_POST['btn_addEvento'])) {
    $id_organizzatore = $_SESSION['LOGIN'];
    $nome = $_POST['add_nome'];
    $descrizione = $_POST['add_descrizione'];
    $data_inizio = $_POST['add_datainizio'];
    $data_fine = $_POST['add_datafine'];
    $data_fine = date('Y-m-d', strtotime($data_fine));
    $data_inizio = date('Y-m-d', strtotime($data_inizio));

    $ora = $_POST['add_ora'];
    $ora = date("H:i", strtotime($ora));
    $prezzo = $_POST['add_prezzo'];
    $disponibilita = $_POST['add_disp'];
    $categoria = $_POST['add_categoria'];
    $indirizzo = $_POST['add_indirizzo'];
    $citta = $_POST['add_citta'];

    if (isset($_FILES['img_evento']) || !is_uploaded_file($_FILES['img_evento']['tmp_name'])) {
        $nomeFoto = addImage('img_evento');
        print_r($nomeFoto);
    } else {
        $_SESSION['status'] = "Errore! Immagine non aggiunta correttamente";
        header('Location: ../eventiOrganizzati');
        exit();
    }
    if ($dbh->addEvento(
        $nome,
        $prezzo,
        $descrizione,
        $data_inizio,
        $data_fine,
        $ora,
        $disponibilita,
        $categoria,
        $nomeFoto,
        $citta,
        $indirizzo
    )) {
        if ($dbh->addEventoInserito($id_organizzatore)) {
            $_SESSION['success'] = "Evento aggiunto correttamente";
        } else {
            $_SESSION['status'] = "Errore! Evento non aggiunto";
        }
        header('Location: ../eventiOrganizzati');
    } else {
        $_SESSION['status'] = "Errore! Evento non aggiunto";
        header('Location: ../eventiOrganizzati');
    }
}

if (isset($_POST['btn_editEvento'])) {
    $id_organizzatore = $_SESSION['LOGIN'];
    $id = $_POST['input_id'];
    $nome = $_POST['add_nome'];
    $descrizione = $_POST['add_descrizione'];
    $data_inizio = $_POST['add_datainizio'];
    $data_fine = $_POST['add_datafine'];
    $data_fine = date('Y-m-d', strtotime($data_fine));
    $data_inizio = date('Y-m-d', strtotime($data_inizio));

    $ora = $_POST['add_ora'];
    $ora = date("H:i", strtotime($ora));
    $prezzo = $_POST['add_prezzo'];
    $disponibilita = $_POST['add_disp'];
    $categoria = $_POST['add_categoria'];
    $indirizzo = $_POST['add_indirizzo'];
    $citta = $_POST['add_citta'];

    // if (isset($_FILES['img_evento']) || !is_uploaded_file($_FILES['img_evento']['tmp_name'])) {
    //     $nomeFoto = addImage('img_evento');
    //     print_r($nomeFoto);
    // } else {
    //     $nomeFoto = $dbh->getEventoById($id)['foto'];
    // }

    if ($dbh->editEvento(
        $id,
        $nome,
        $prezzo,
        $descrizione,
        $data_inizio,
        $data_fine,
        $ora,
        $disponibilita,
        $categoria,
        $citta,
        $indirizzo
    )) {
        if ($dbh->addEventoInserito($id_organizzatore)) {
            $_SESSION['success'] = "Evento modificato correttamente";
        } else {
            $_SESSION['status'] = "Errore! Evento non modificato";
        }
        header('Location: ../eventiOrganizzati');
    } else {
        $_SESSION['status'] = "Errore! Evento non modificato";
        header('Location: ../eventiOrganizzati');
    }
}
