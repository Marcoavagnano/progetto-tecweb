<?php
require("../utils.php");
// setcookie("cart", "", time()-3600, "/"); // empty value and old timestamp
// unset($_COOKIE["cart"]);

if (!isset($_SESSION))
    session_start();
$cart_id = $_POST["cart_id"];

$tempoCarrello = 600;
$row = $dbh->getEventoById($cart_id);

if (!isset($_COOKIE["cart"])) {
    $cart_data = array();
} else {
    $cookie_data = stripslashes($_COOKIE['cart']);
    $cart_data = json_decode($cookie_data, true);
}

$item_id_list = array_column($cart_data, 'item_id');

if (in_array($row["id"], $item_id_list)) {
    foreach ($cart_data as $keys => $values) {
        if ($cart_data[$keys]["item_id"] == $row["id"]) {
            $cart_data[$keys]["item_quantity"] = $cart_data[$keys]["item_quantity"] + 1;
        }
    }
} else {
    $item_array = array(
        'item_id'   => $row["id"],
        'item_quantity'  => 1
    );
    $cart_data[] = $item_array;
}

$item_data = json_encode($cart_data);
setcookie('cart', $item_data, time() + $tempoCarrello, "/");
//header('Location: ' . $_SERVER['HTTP_REFERER']);

// $item_array = array(
//     'item_id' => $row["id"],
//     'item_quantity' => 1
// );
// array_push($result, $row["id"]);
// setcookie("cart", json_encode($result), time() + $tempoCarrello, "/");
// print_r(json_decode($_COOKIE["cart"], true));
