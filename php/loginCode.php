<?php
session_start();
require("../utils.php");
$email = $_POST["inputemail"];
$pass = $_POST["inputpass"];

if (isset($_SESSION["MANAGER"])) {
    $id = $dbh->getIdManagerByEmail($email);
    $result = $dbh->logManager($email, $pass);
} else {
    $id = $dbh->getIdUserByEmail($email);
    $result = $dbh->logUser($email, $pass);
}
if (!$result) {
    $_SESSION['errLog'] = "Email o password errati!";
    header("Location: ../login");
} else {
    if (isset($_POST["loginTrue"])) {
        $tempoLogin = 2147483647;
        setcookie("LOGIN", $id, $tempoLogin, "/");
    } else {
        $_SESSION["LOGIN"] = $id;
    }
    if ($_SESSION["MANAGER"]) {
        header("Location: ../eventiOrganizzati");
    } else {
        header("Location: ../index");
    }
}
