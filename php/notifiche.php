<?php
session_start();
include('../utils.php');
//prendere eventi dall id utente
//codificare in jsonse l' evento sta per iniziare o va in soldout
if (isset($_SESSION["LOGIN"])) {
    $id = $_SESSION["LOGIN"];
    $righe = 0;
    $output = '';

    if (isset($_SESSION["MANAGER"])) {
        $eventi = $dbh->getEventiByOrg($id);
        $sum = array();
        foreach ($eventi as $evento) {
            $sum[$evento] = $dbh->contaEventiCompratiInTotale($evento);
        }
        foreach ($sum as $evento_id => $value) {
            $evento = $dbh->getEventoById($evento_id);
            if ($value ==  $evento['disponibilita']) {
                $righe++;
                $output .= '
                    <a class="dropdown-item">' . $evento["nome"] . ' soldout!' . ' </a>';
            }
        }
    } else {
        $eventi = array();
        $eventi = $dbh->getEventiByUtente($id);
        //eventi che stanno per scadere
        //data dell evento - data di oggi se e' uguale a 24 ore o meno notifica
        if (!empty($eventi)) {
            foreach ($eventi as $evento) {
                $evento =  $dbh->getEventoById($evento);
                $daydiff = floor((abs(strtotime(date("Y-m-d")) - strtotime($evento['data_inizio'])) / (60 * 60 * 24)));
                //if ($evento['data'] >= date('Y-m-d') && time() >= strtotime($evento['ora'])) {
                if ($daydiff <= 1) {
                    $righe++;
                    $output .= '
            <a class="dropdown-item" href="dettaglioEvento?id_evento=' . $evento["id"] . '"> '  . $evento["nome"] . ' sta per iniziare!' . ' </a>';
                }
            }
        }
    }

    if (!$righe)
        $output .= '<a class="dropdown-item">Non ci sono notifiche</a>';

    $data = array(
        'notification' => $output,
        'count' => $righe
    );
    echo json_encode($data);
}
