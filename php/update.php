<?php
if (!isset($_SESSION))
session_start();
require("../utils.php");
$name = $_POST["regname"];
$surname = $_POST["regsurname"];
$date = $_POST["regdate"];
$email = $_POST["regemail"];
$address = $_POST["regaddress"];
$password = $_POST["regpass"];
$cpassword = $_POST["regconfpass"];
if (isset($_COOKIE["LOGIN"]))
    $idUtente = $_COOKIE["LOGIN"];
if (isset($_SESSION["LOGIN"])) 
    $idUtente = $_SESSION["LOGIN"];
if (($password ===  $cpassword)) {
    $passOk = true;
} else {
    $passOk = false;
    $return = "Le password non coincidono";
    $_SESSION['errUpdate'] = $return;
}
if (
    empty($name) || empty($surname) || empty($date) || empty($email) ||
    empty($address)) {
    $return = "Riempi ogni campo correttamente";
    $_SESSION['errSignup'] = $return;
}else{
if ($passOk){
    if(!isset($_SESSION["MANAGER"])){
        
        $dbh->updateUtente($idUtente, $name, $surname, $date, $email, $address, $password);
    }else{
        $partita_iva=$_POST["regiva"];
        $dbh->updateOrganizzatore($idUtente, $name, $surname, $date, $email, $address, $password,$partita_iva);
    }
}   
}
unset($idUtente);
header("Location: ../myprofile");
?>