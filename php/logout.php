<?php
session_start();
if (isset($_COOKIE["LOGIN"])) {    
    setcookie("LOGIN", "", time()-3600, "/"); // empty value and old timestamp
    unset($_COOKIE["LOGIN"]);
} else {
    unset($_SESSION["LOGIN"]);
    session_destroy();
}
if(isset($_COOKIE["cart"])){
    setcookie("cart", "", time()-3600, "/"); // empty value and old timestamp
    unset($_COOKIE["cart"]);
}
header("Location: ../index.php");
