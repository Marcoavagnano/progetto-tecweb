<?php
include('header.php');

?>
<div class="container-fluid">
   <div class="row justify-content-center">
      <div class="col-lg-5">
         <div class="card-body px-lg-5 py-lg-5">
            <div class="text-center mb-4">
               <h4> Accedi</h4>
            </div>
            <form action="php/loginCode.php" method="post">
               <div class="form-group">
                  <label>
                     <h6>Email </h6>
                  </label>
                  <input class="form-control inputData" name="inputemail" type="email">
               </div>
               <div class="form-group">
                  <label>
                     <h6>Password </h6>
                  </label>
                  <input class="form-control inputData" name="inputpass" type="password">
               </div>
               <div class="form-group">
                  <input type="checkbox" name="loginTrue"> Mantienimi connesso
               </div>
               <button class="btn  btn-default  btn-block" type="submit">Invia</button>
               <?php
               if (isset($_SESSION['errLog'])) {
                  echo   '<div class="alert alert-danger mt-3" role="alert">';
                  echo $_SESSION['errLog'];
                  echo "</div>";
                  unset($_SESSION['errLog']);
               }
               ?>

            </form>
         </div>
      </div>
   </div>
</div>

<?php
include('footer.php');
?>