<?php
session_start();
include('utils.php');
include('header.php');
?>
<div class="container ">
    <!-- <div class="row float-right">
            <a href="aggiungiEvento.php">
                <button class="btn  btn-default sub mt-3 mr-3">Aggiungi evento</button>
            </a>
        </div> -->
    <!-- Jumbotron -->
    <div class="jumbotron">
        <div class="row">
             <div class="col col-6">
                <h1>Tutti gli eventi!</h1>
             </div>
             <div class="col col-6">
               
            </div>
       
    </div>
    </div>
    <div class="mb-4">
    <form class="form-inline md-form form-sm" action="php/ricercaEvento.php" method="post" >
                    <input class="form-control form-control-sm w-50"  name="evento_cercato" type="text" placeholder="Cerca"
                    aria-label="Search">
                    <button class="btn" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
                <?php
                if(isset($_SESSION['ricerca']) && !empty($_SESSION['ricerca']))
                    echo '<a href="listaEventi">Visualizza tutti gli eventi</a>';
                    ?>
    </div>

    <!-- Example row of columns -->
    <div class="row">
        <?php
       $dbEvents=$dbh->getEventi();
       
        if(isset($_SESSION['ricerca']) && !empty($_SESSION['ricerca'])){
            $searchArray=array();
            $substr=strtoupper($_SESSION['ricerca']);
          
            foreach ($dbEvents as $row) {
                $nome=strtoupper($row['nome']);
                
                if(!(strpos($nome,$substr)===false)){
          
                    array_push($searchArray,$row);
                }
            }
            $dbEvents=$searchArray;
            unset($_SESSION['ricerca']);
        }
             
        foreach ($dbEvents as $row) { ?>
            <div class="col-lg-4">

                <img src="<?php echo $dbh->getImgSrc($row['foto']); ?>" class="img-thumbnail" alt="Foto Evento">
                <h2><?php echo $row['nome'] ?></h2>
                <p id="descrizione_evento"><?php echo $row['descrizione'] ?></p>
                <form action="dettaglioEvento" method="GET">
                    <input type='hidden' name='id_evento' value='<?php echo $row['id'] ?>'>
                    <p><button class="btn btn-primary">Vai all' Evento &raquo;</button></p>
                </form>
            </div>
        <?php }

        ?>
    </div>

</div>
<?php
include('footer.php');
?>