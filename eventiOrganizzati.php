<?php
session_start();
if (isset($_SESSION['LOGIN'])) :
    require("utils.php");
    include('header.php');
    include('utils.php');
?>
    <section class="section">
        <div class="container" id="desktopEventiOrg">
            <?php
            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                echo "<div class='alert alert-success' role='alert'>" . $_SESSION['success'] . "</div>";
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                echo "<div class='alert alert-danger' role='alert'>" . $_SESSION['status'] . "</div>";
                unset($_SESSION['status']);
            }
            ?>
            <div class="row align-items-center justify-content-center">
                <h1>Eventi Organizzati</h1>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered cellspacing=" 0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Data Inizio</th>
                            <th scope="col">Data Fine</th>
                            <th scope="col">Prezzo</th>
                            <th scope="col">Disponibilità</th>
                            <th scope="col">Biglietti comprati</th>
                        </tr>
                    </thead>
                    <?php
                    $id_organizzatore = $_SESSION['LOGIN'];
                    foreach ($dbh->getEventiByOrganizzatore($id_organizzatore) as $eventi) {
                        $evento = $dbh->getEventoById($eventi['id_evento']);
                    ?>
                        <tr>
                            <td>
                                <a href="dettaglioEvento?id_evento=<?php echo $evento['id']; ?> ">
                                    <img src=" <?php echo $dbh->getImgSrc($evento['foto']) ?>" alt="Immagine Evento" class="img-fluid rounded shadow" style="width: 200px;">
                                </a>
                            </td>
                            <td>
                                <?php echo $evento['nome']; ?>
                            </td>
                            <td>
                                <?php echo  date('d-m-Y', strtotime($evento['data_inizio'])) ?>
                            </td>
                            <td>
                                <?php echo  date('d-m-Y', strtotime($evento['data_fine'])); ?>
                            </td>
                            <td>
                                <?php echo $evento['prezzo'] . "€"; ?>
                            </td>
                            <td>
                                <?php echo $evento['disponibilita']; ?>
                            </td>
                            <td>
                                <?php if ($dbh->contaEventiCompratiInTotale($evento['id']) == "")
                                    echo 0;
                                else echo $dbh->contaEventiCompratiInTotale($evento['id']); ?>
                            </td>
                            <td>
                                <a href="modificaEvento?idEvento=<?php echo $evento['id']; ?> "><button class="btn btn-primary">MODIFICA</button></a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </div>
        </div>
        <div class="container" id="mobileOrganizzati">
            <?php
            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                echo "<div class='alert alert-success' role='alert'>" . $_SESSION['success'] . "</div>";
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                echo "<div class='alert alert-danger' role='alert'>" . $_SESSION['status'] . "</div>";
                unset($_SESSION['status']);
            }
            ?>
            <!-- For demo purpose -->
            <div class="row text-center text-white mb-5">
                <div class="col-lg-7 mx-auto">
                    <h1 class="display-4">Eventi Organizzati</h1>
                </div>
            </div>
            <!-- End -->

            <div class="row">
                <div class="col-lg-8 mx-auto">

                    <!-- List group-->
                    <ul class="list-group shadow">
                        <?php
                        $id_organizzatore = $_SESSION['LOGIN'];
                        foreach ($dbh->getEventiByOrganizzatore($id_organizzatore) as $eventi) {
                            $evento = $dbh->getEventoById($eventi['id_evento']);
                        ?>
                            <!-- list group item-->
                            <li class="list-group-item">
                                <div class="row justify-content-end mr-1">
                                    <a href="modificaEvento?idEvento=<?php echo $evento['id']; ?> "><i class="fa fa-pencil alt"></i></a>
                                </div>
                                <!-- Custom content-->
                                <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                                    <div class="media-body order-2 order-lg-1">

                                        <h5 class="mt-0 font-weight-bold mb-2"><?php echo $evento['nome']; ?></h5>
                                        <p class="font-italic text-muted mb-0 small"><?php echo $evento['descrizione']; ?></p>
                                        <div class="d-flex align-items-center justify-content-between mt-1">
                                            <h6 class="font-weight-bold my-2"><?php echo $evento['prezzo'] . "€"; ?></h6>
                                            <ul class="list-inline small">
                                                <li class="list-inline-item m-0"><?php if ($dbh->contaEventiCompratiInTotale($evento['id']) == "")
                                                                                        echo "Biglietti Acquistati: 0";
                                                                                    else echo "Biglietti Acquistati: " . $dbh->contaEventiCompratiInTotale($evento['id']); ?></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <img src=" <?php echo $dbh->getImgSrc($evento['foto']) ?>" alt="Immagine Evento" width="200" class="ml-lg-5 order-1 order-lg-2">
                                </div>
                                <!-- End -->
                            </li>
                        <?php
                        }
                        ?>
                        <!-- End -->
                    </ul>
                    <!-- End -->
                </div>
            </div>
        </div>
    </section>
<?php
    include('footer.php');
else :
    // Redirect them to the login page
    header('Location: login.php');
    die();
endif;
?>