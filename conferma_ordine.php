<?php
require("utils.php");
// setcookie("cart", "", time()-3600, "/"); // empty value and old timestamp
// unset($_COOKIE["cart"]);

if (!isset($_SESSION))
    session_start();

if (isset($_SESSION["LOGIN"])) {
    if(empty($_COOKIE['cart'])){
        $_SESSION['empty_cart']="Aggiungi almeno un evento al carrello";
        header("Location: carrello");
        exit();
    }
    $item_id_list=array();
    $cookie_data = stripslashes($_COOKIE['cart']);
   
    $cart_data = json_decode($cookie_data, true);
   foreach($cart_data as $keys){
    array_push( $item_id_list,$keys);
   }

    $id_utente = $_SESSION["LOGIN"];
    $dbh->addOrdine($item_id_list, $id_utente);
    setcookie("cart", "", time()-3600, "/"); // empty value and old timestamp
    unset($_COOKIE["cart"]);
    header("Location: index");
} else {
    header("Location: login");
}
