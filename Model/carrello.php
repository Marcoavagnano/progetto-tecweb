<?php
class Carrello{
private $listaEventi;
private $totCosto;

public function __construct(){
    $listaEventi=array();
    $totCosto=0.00;
}
public function getEventi(){
    return $this->listaEventi;
}
public function getCosto(){
    return $this->totCosto;
}

public function aggiungiEvento($evento){
    array_push($this->listaEventi,$evento);
}

public function rimuoviEvento($id)
{
    if(array_key_exists($id, $this->listaEventi))
    {
        unset($this->listaEventi[$id]);
    }
}
public function carrelloVuoto(){
    return empty($this->listaEventi);
}
}


?>