<?php
class Utente{
private $id;
private $nome;
private $cognome;
private $dataNascita;
private $email;


public function __construct($id, $nome, $cognome, $dataNascita, $email){
        $this->id = $id;
        $this->nome = $nome;
        $this->cognome = $cognome;
        $this->dataNascita = $dataNascita;
        $this->email = $email;
}
public function getId(){
    return $this->id;
}
public function getNome(){
    return $this->nome;
}
public function getCognome(){
    return $this->cognome;
}
public function getDataNascita(){
    return $this->dataNascita;
}
public function getEmail(){
    return $this->email;
}


}
?>