<?php
class Evento{
private $id;
private $nome;
private $prezzo;
private $descrizione;
private $dataInizio;
private $dataFine;
private $ora;
private $disponibilita;
private $categoria;
private $linkFoto;
private $luogo;

public function __construct($id,$nome,$prezzo,$descrizione,$dataInizio,$dataFine,$ora,$disponibilita,$categoria,$linkFoto,$luogo){
    $this->id;
    $this->nome;
    $this->prezzo;
    $this->descrizione;
    $this->dataInizio;
    $this->dataFine;
    $this->ora;
    $this->disponibilita;
    $this->categoria;
    $this->linkFoto;
    $this->luogo;
}

public function getId(){
    return $this->id;
}

public function getNome(){
    return $this->nome;
}

public function getPrezzo(){
    return $this->prezzo;
}
public function getDescrizione(){
    return $this->descrizione;
}
public function getDataInizio(){
    return $this->dataInizio;
}
public function getDataFine(){
    return $this->dataFine;
}
public function getOra(){
    return $this->ora;
}
public function getDisponibilita(){
    return $this->disponibilita;
}
public function getCategoria(){
    return $this->categoria;
}
public function getLinkFoto(){
    return $this->linkFoto;
}
public function getLuogo(){
    return $this->luogo;
}
public function aggiungiEventoDb(){
    
}



}

?>