<?php
ob_start();
if (!isset($_SESSION))
  session_start();
include('utils.php');
include('header.php');
if (isset($_SESSION['LOGIN'])) :
  $arrayEventi=json_decode($_COOKIE['cart'],true);
  if (!isset($_COOKIE['cart']) || sizeof($arrayEventi)<=0) {
  
    $_SESSION['empty_cart'] = "Aggiungi almeno un elemento al carrello";
    header("Location: carrello");
    exit();
  }
?>
  <div class="container ">


    <!-- Example row of columns -->
    <div class="row">


      <div class="col-lg-6">

        <div class="m-5 text-center">
          <h5><strong>RIEPILOGO ORDINE</strong></h5>
          <?php
          $totale = 0;
          foreach (json_decode($_COOKIE["cart"], true) as $keys) :
            $row = $dbh->getEventoById($keys["item_id"]);
            $quantita = intval($keys["item_quantity"]);
            $prezzo = intval($row['prezzo']);
            $totale += ($prezzo * $quantita);
            echo '<h5>' . $quantita . ' x '  . $row['nome'] . ' €' . $row['prezzo'] .  '</h5>';
          endforeach;


          ?>


        </div>
      </div>
      <div class="col-lg-6 ">
        <div class="m-5 text-center">
          <h5><strong>PAGAMENTO CON PAYPAL</strong></h5>
          <p><img src="res/img/paypal.png" style="max-width: 40%;" class="img-fluid rounded shadow-sm"></p>
          <p>
            <h5>Totale da pagare: €<?php echo $totale ?></h5>
          </p>

          <button id="pagamentobtn" data-toggle="modal" data-target="#pagamentoModal" class="btn btn-primary">Conferma e Paga</button>

        </div>
      </div>
    </div>

  </div>
  <div class="modal" id="pagamentoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <a href="carrello" class="close" data-target="carrello" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </a>
        </div>
        <div class="modal-body">
          PAGAMENTO EFFETTUATO CON SUCCESSO
          <i class="ni ni-check-bold"></i>
        </div>
        <div class="modal-footer">
          <a href="carrello" data-target="carrello" class="btn btn-secondary">Ok!</a>
          <a href="index" data-target="index" class="btn btn-primary">Vai alla HOME!</a>
        </div>
      </div>
    </div>
  </div>
<?php

  include('footer.php');
else :
  header("Location: login");
  exit();
endif;
?>