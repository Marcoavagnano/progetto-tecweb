<?php
class DatabaseHelper
{
    private $mysqli;
    public function __construct($servername, $username, $password, $dbname)
    {
        $this->mysqli = new mysqli($servername, $username, $password, $dbname);
        if ($this->mysqli->connect_error) {
            die("Connection failed: " . $this->mysqli->connect_error);
        }
    }

    public function addAdmin($username, $email, $password)
    {
        $sql = "INSERT INTO Admin (id, username, email, password)
        VALUES (NULL, ?,?,?)";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('sss', $username, $email,  $password);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }

    public function getTotalAdmin()
    {
        $sql = "SELECT * FROM Admin";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return;
        }
        return $result->num_rows;
    }

    public function getProfilesTable()
    {
        $sql = "SELECT * FROM Anagrafica, Utenti WHERE Anagrafica.id = Utenti.id_anagrafica";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return;
        }
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["nome"] . "</td>";
                echo "<td>" . $row["cognome"] . "</td>";
                echo "<td>" . $row["email"] . "</td>";
                echo "<td><input style='border: 0; background-color: #fff;' type='password' value='" . $row['password'] . "'readonly='readonly'>
                </td>";
                echo "<td>" . $row["indirizzo"] . "</td>";
                echo "<td>" . $row["data_nascita"] . "</td>";
                echo "<td>";
                echo "<form action='editProfile.php' method='post'>";
                echo "<input type='hidden' name='edit_id' value='{$row['id']}' >";
                echo "<button type='submit' name='edit_btn' class='btn btn-success'>MODIFICA</button>";
                echo "</td>";
                echo "<td>";
                echo "<button type='submit' name='delete_btn' class='btn btn-danger'>ELIMINA</button></form>";
                echo "</td>";
                echo "</tr>";
            }
        }
    }

    public function getOrgTable()
    {
        $sql = "SELECT * FROM Anagrafica, Organizzatori WHERE Anagrafica.id = Organizzatori.id_anagrafica";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return;
        }
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["nome"] . "</td>";
                echo "<td>" . $row["cognome"] . "</td>";
                echo "<td>" . $row["email"] . "</td>";
                echo "<td><input style='border: 0; background-color: #fff;' type='password' value='" . $row['password'] . "'readonly='readonly'>
                </td>";
                echo "<td>" . $row["indirizzo"] . "</td>";
                echo "<td>" . $row["data_nascita"] . "</td>";
                echo "<td>";
                echo "<form action='editProfile.php' method='post'>";
                echo "<input type='hidden' name='edit_id' value='{$row['id']}' >";
                echo "<button type='submit' name='edit_btn' class='btn btn-success'>MODIFICA</button>";
                echo "</td>";
                echo "<td>";
                echo "<button type='submit' name='delete_btn' class='btn btn-danger'>ELIMINA</button></form>";
                echo "</td>";
                echo "</tr>";
            }
        }
    }

    public function deleteUtente($id)
    {
        $sqlA = "SELECT id_anagrafica FROM Utenti WHERE id = ?";
        $stmt = $this->mysqli->prepare($sqlA);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_anagrafica = $row["id_anagrafica"];
        $sql = "DELETE FROM Utenti WHERE id = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $sql = "DELETE FROM Anagrafica WHERE id = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id_anagrafica);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }

    public function getCategorieTable()
    {
        $sql = "SELECT * FROM Categorie";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
        }
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["nome"] . "</td>";
                echo "<td>";
                echo "<form action='addCategoria.php' method='post'>";
                echo "<input type='hidden' name='nome' value='{$row['nome']}' >";
                echo "<button type='submit' name='eliminaCategoria' class='btn btn-danger'>ELIMINA</button></form>";
                echo "</td>";
                echo "</tr>";
            }
        }
    }

    public function contaEventiCompratiInTotale($id_evento)
    {
        $sql = "SELECT SUM(quantita) FROM Dettaglio_Ordine WHERE id_evento = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id_evento);

        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        return $result->fetch_assoc()['SUM(quantita)'];
    }

    public function getEventiByOrg($id_org)
    {
        $sql = "SELECT id_evento FROM EventiInseriti WHERE id_organizzatore = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id_org);

        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $eventi = array();
        while ($row = $result->fetch_assoc()) {
            $eventi[] = $row["id_evento"];
        }
        return $eventi;
    }

    public function deleteCategoria($nome)
    {
        $sql = "DELETE FROM Categorie WHERE nome = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $nome);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }


    public function getEventiTable()
    {
        $sql = "SELECT id, nome, data_inizio, data_fine, ora, luogo FROM Eventi";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
        }
        if ($result->num_rows > 0) {
            echo '<table class="table table-bordered" cellspacing="0" id="eventiTabella">';
            echo "<thead>";
            echo "<tr>";
            echo "<th>ID</th>";
            echo "<th>Nome</th>";
            echo "<th>Data Inizio</th>";
            echo "<th>Data Fine</th>";
            echo "<th>Ora</th>";
            echo "<th>Luogo</th>";
            echo "<th>DETTAGLIO</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["nome"] . "</td>";
                echo "<td>" . $row["data_inizio"] . "</td>";
                echo "<td>" . $row["data_fine"] . "</td>";
                echo "<td>" . $row["ora"] . "</td>";
                echo "<td>" . $this->getLuogoById($row["luogo"])["citta"] . "</td>";
                echo "<td>";
                echo "<form action='dettaglioEvento.php' method='GET'>";
                echo "<input type='hidden' name='edit_id' value='{$row['id']}' >";
                echo "<button type='submit' name='edit_btn' class='btn btn-success'>Dettaglio Evento</button></form>";
                echo "</td>";
                echo "</tr>";
            }
            echo "</tbody>";
            echo '</table>';
        }
    }

    public function getAdminsTable()
    {
        $sql = "SELECT * FROM Admin";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
        }
        if ($result->num_rows > 0) {
            echo '<table class="table table-bordered" cellspacing="0" id="adminTable">';
            echo "<thead>";
            echo "<tr>";
            echo "<th>ID</th>";
            echo "<th>Username</th>";
            echo "<th>Email</th>";
            echo "<th>Password</th>";
            echo "<th>MODIFICA</th>";
            echo "<th>ELIMINA</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["username"] . "</td>";
                echo "<td>" . $row["email"] . "</td>";
                echo "<td>" . $row["password"] . "</td>";
                echo "<td>";
                echo "<form action='editAdmin.php' method='post'>";
                echo "<input type='hidden' name='edit_id' value='{$row['id']}' >";
                echo "<button type='submit' name='edit_btn' class='btn btn-success'>MODIFICA</button>";
                echo "</td>";
                echo "<td>";
                echo "<button type='submit' name='delete_btn' class='btn btn-danger'>ELIMINA</button>";
                echo "</td>";
                echo "</form>";
                echo "</tr>";
            }
            echo "</tbody>";
            echo '</table>';
        }
    }

    public function getOra()
    {
        $sql = "SELECT UNIX_TIMESTAMP(ora) FROM orario";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            die('Invalid query: ' . $this->mysqli->error);
            return;
        } else {
            while ($row = $result->fetch_assoc())
                $results[] = $row["UNIX_TIMESTAMP(ora)"];
        }
        return $results;
    }

    public function getOraByEvento($id)
    {
        $sql = "SELECT UNIX_TIMESTAMP(ora) FROM orario";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            die('Invalid query: ' . $this->mysqli->error);
            return;
        } else {
            while ($row = $result->fetch_assoc())
                $results[] = $row["UNIX_TIMESTAMP(ora)"];
        }
        return $results;
    }

    public function addprovaOra($ora)
    {
        $sql = "INSERT INTO orario (ora) VALUES (?)";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $ora);

        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }

        return true;
    }

    public function getLuoghiTable()
    {
        $sql = "SELECT Luoghi.* FROM Luoghi";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
        }
        if ($result->num_rows > 0) {
            echo '<table class="table table-bordered" cellspacing="0" id="luoghiTable">';
            echo "<thead>";
            echo "<tr>";
            echo "<th>ID</th>";
            echo "<th>Indirizzo</th>";
            echo "<th>Città</th>";
            echo "<th>MODIFICA</th>";
            echo "<th>ELIMINA</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["indirizzo"] . "</td>";
                echo "<td>" . $row["citta"] . "</td>";
                echo "<td>";
                echo "<form action='editLuogo.php' method='post'>";
                echo "<input type='hidden' name='edit_id' value='{$row['id']}' >";
                echo "<button type='submit' name='edit_btn' class='btn btn-success'>MODIFICA</button>";
                echo "</td>";
                echo "<td>";
                echo "<button type='submit' name='delete_btn' class='btn btn-danger'>ELIMINA</button>";
                echo "</td>";
                echo "</form>";
                echo "</tr>";
            }
            echo "</tbody>";
            echo '</table>';
        }
    }

    public function getLuogoById($id)
    {
        $sql = "SELECT Luoghi.* FROM Luoghi WHERE Luoghi.id = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function updateAdmin($id, $username, $email, $password)
    {
        $sql = "UPDATE Admin SET username = ?, email = ?, password = ? WHERE id = ? ";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('sssi', $username, $email, $password, $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }

    public function deleteAdminById($id)
    {
        $sql = "DELETE FROM Admin WHERE ? = id";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }

    public function addEventoInserito($id_org)
    {
        $sql = "SELECT id FROM Eventi ORDER BY id DESC LIMIT 1";
        $result = $this->mysqli->query($sql);
        $id_evento = $result->fetch_assoc();
        $sql = "INSERT INTO EventiInseriti (id_organizzatore, id_evento) VALUES (?, ?)";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('ii', $id_org, $id_evento['id']);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }

    public function getOrdiniTable()
    {
        $sql = "SELECT * FROM Ordini";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();
        $count = 1;
        if ($result->num_rows > 0) {
            echo '<table class="table table-bordered" cellspacing="0" id="ordiniTable">';
            echo "<thead>";
            echo "<tr>";
            echo "<th>#</th>";
            echo "<th>Utente</th>";
            echo "<th>Data</th>";
            echo "<th>Ora</th>";
            echo "<th>Dettaglio</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["utente_id"] . "</td>";
                echo "<td>" . $row["data"] . "</td>";
                echo "<td>" . $row["ora"] . "</td>";
                echo "<td>";
                echo "<form action='dettaglioOrdine' method='POST'>";
                echo "<input type='hidden' name='edit_id' value='{$row['id']}' >";
                echo "<button type='submit' name='edit_btn' class='btn btn-success'>DETTAGLIO ORDINE</button>";
                echo "</td>";
                echo "</form>";
                echo "</tr>";
                $count++;
            }
            echo "</tbody>";
            echo '</table>';
        }
    }

    public function getOrdiniTableByUtente($id)
    {
        $sql = "SELECT id, data, ora FROM Ordini WHERE ? = utente_id";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $count = 1;
        if ($result->num_rows > 0) {
            echo '<table class="table table-bordered table-hover" cellspacing="0" id="ordiniTable">';
            echo "<thead>";
            echo "<tr>";
            echo "<th>#</th>";
            echo "<th>Data</th>";
            echo "<th>Ora</th>";
            echo "<th>Dettaglio Ordine</th>";
            echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $count . "</td>";
                echo "<td>" . date('d-m-Y', strtotime($row["data"])) . "</td>";
                echo "<td>" . $row["ora"] . "</td>";
                echo "<td>";
                echo "<form action='dettaglioOrdine.php' method='POST'>";
                echo "<input type='hidden' name='edit_id' value='{$row['id']}' >";
                echo "<button type='submit' name='edit_btn' class='btn btn-success'>DETTAGLIO ORDINE</button>";
                echo "</td>";
                echo "</form>";
                echo "</tr>";
                $count++;
            }
            echo "</tbody>";
            echo '</table>';
        }
    }

    public function getDettaglioOrdineByIdOrdine($id)
    {
        $sql = "SELECT * FROM Dettaglio_Ordine WHERE ? = id_ordine";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getEventiByOrganizzatore($id)
    {
        $sql = "SELECT * FROM EventiInseriti WHERE ? = id_organizzatore";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCitta()
    {
        $sql = "SELECT Citta.nome FROM Citta";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            die('Invalid query: ' . $this->mysqli->error);
            return;
        } else {
            while ($row = $result->fetch_assoc())
                $results[] = $row["nome"];
        }
        return $results;
    }

    public function getLuoghi()
    {
        $sql = "SELECT indirizzo FROM Luoghi";
        $result = $this->mysqli->query($sql);
        if (!$result) {
            die('Invalid query: ' . $this->mysqli->error);
            return;
        } else {
            while ($row = $result->fetch_assoc())
                $results[] = $row["indirizzo"];
        }
        return $results;
    }

    public function deleteLuogoById($id)
    {
        $sql = "DELETE FROM Luoghi WHERE id = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }

    public function getAdminById($id)
    {
        $sql = "SELECT * FROM Admin WHERE id = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return $stmt->get_result();
    }

    public function getOrdiniByUtente($id_utente)
    {
        $stmt = $this->mysqli->prepare("SELECT id FROM Ordini WHERE ? = utente_id");
        $stmt->bind_param('i', $id_utente);
        $stmt->execute();
        $result = $stmt->get_result();
        $ordini = array();
        while ($row = $result->fetch_assoc()) {
            $ordini[] = $row["id"];
        }
        return $ordini;
    }

    public function getEventiByUtente($id_utente)
    {
        $ordini = $this->getOrdiniByUtente($id_utente);
        if (!empty($ordini)) {
            foreach ($ordini as $ordine) {
                $stmt = $this->mysqli->prepare("SELECT id_evento FROM Dettaglio_Ordine WHERE ? = id_ordine");
                $stmt->bind_param('i', $ordine);
                $stmt->execute();
                $result = $stmt->get_result();
                while ($row = $result->fetch_assoc()) {
                    $eventi[] = $row["id_evento"];
                }
            }
            return $eventi;
        } else
            return array();
    }

    public function getIdByNomeCategoria($categoria)
    {
        $stmt = $this->mysqli->prepare("SELECT id FROM Categorie WHERE ? = nome");
        $stmt->bind_param('s', $categoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc()["id"];
    }


    public function getEventiByCategoria($categoria)
    {
        $stmt = $this->mysqli->prepare("SELECT Eventi.id, Eventi.nome, Eventi.descrizione, Eventi.foto FROM Eventi, Categorie WHERE ? = Eventi.categoria AND Categorie.id = ?");
        $stmt->bind_param('ii', $categoria, $categoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventoById($id)
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM Eventi WHERE ? = id");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_assoc();
    }

    public function getTopEventi()
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM Eventi ORDER BY RAND()");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomEventi()
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM Eventi ORDER BY RAND()");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getIndirizzoCitta($id)
    {
        if (!($stmt = $this->mysqli->prepare("SELECT Luoghi.indirizzo, Luoghi.citta FROM Luoghi WHERE ? = Luoghi.id"))) {
            echo "Prepare failed: (" .  $this->mysqli->errno . ") " .  $this->mysqli->error;
        }
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getImgSrc($id)
    {

        $sql = "SELECT * FROM Immagini WHERE id = ?";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $result = $stmt->get_result();
        $path = $result->fetch_assoc()['link'];
        return $path = substr($path, strpos($path, "res"));
    }

    public function getRandomPosts($n)
    {
        $stmt = $this->db->prepare("SELECT idarticolo, titoloarticolo, imgarticolo FROM articolo ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function logUser($email, $pass)
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM Utenti 
            WHERE email = ?");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $res = $stmt->get_result();
        $row = $res->fetch_assoc();
        if (isset($row)) {

            $dbPass = $row['password'];
            if (password_verify($pass, $dbPass)) {

                $return = true;
            } else {
                $return = false;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    public function logManager($email, $pass)
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM Organizzatori 
            WHERE email = ?");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $res = $stmt->get_result();
        $row = $res->fetch_assoc();
        if (isset($row)) {

            $dbPass = $row['password'];
            if (password_verify($pass, $dbPass)) {

                $return = true;
            } else {
                $return = false;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    public function getEventi()
    {
        $query = "SELECT * FROM Eventi ORDER BY Eventi.nome ASC";
        $result = $this->mysqli->query($query);
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getCategorie()
    {
        $result = $this->mysqli->query("SELECT * FROM Categorie ORDER BY Categorie.nome ASC");
        while ($row = $result->fetch_assoc()) {
            $categories[] = $row["nome"];
        }
        return $categories;
    }
    public function getCategoriaById($id)
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM Categorie WHERE id= ?");
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return "";
        }
        $result = $stmt->get_result();
        return ($result->fetch_assoc())['nome'];
    }

    public function addImageToDb($name, $path)
    {
        $sql = "INSERT INTO Immagini (id, nome, link) VALUES (NULL, ?, ?)";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('ss', $name, $path);

        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }

        return true;
    }

    public function getEventiPerCategoria($idCategoria)
    {
        $stmt = $this->mysqli->prepare("SELECT * FROM Eventi INNER JOIN Categorie 
                                        ON Eventi.categoria = Categorie.?");
        $stmt->bind_param('s', $idCategoria);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function getAdminByUsername($username)
    {
        $returnT = false;
        $stmt = $this->mysqli->prepare("SELECT * FROM Admin WHERE username = ?");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        $user = $result->fetch_assoc();
        return  $user;
    }

    public function getProfileById($id)
    {
        $sql = "SELECT * FROM Utenti, Anagrafica WHERE ? = Utenti.id AND Utenti.id_anagrafica = Anagrafica.id";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        return ($stmt->get_result())->fetch_all(MYSQLI_ASSOC);
    }

    public function addUser($name, $surname, $date, $email, $address, $pass)
    {
        $hash_pass = password_hash($pass, PASSWORD_DEFAULT);

        if (!$this->addPersona($name, $surname, $date, $address)) {
            return false;
        }
        $sql = "SELECT id FROM Anagrafica ORDER BY id DESC LIMIT 1";
        $result = $this->mysqli->query($sql);
        $row = $result->fetch_assoc();
        $stmt = $this->mysqli->prepare("INSERT INTO Utenti (id, email, password, id_anagrafica)
            VALUES (NULL, ?, ?, ?)");
        $stmt->bind_param('sss', $email, $hash_pass, $row["id"]);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        return true;
    }

    public function addPersona($name, $surname, $date, $address)
    {
        $stmt = $this->mysqli->prepare("INSERT INTO Anagrafica (id, nome, cognome, data_nascita,indirizzo)
        VALUES (NULL, ?, ?,?, ?)");
        $stmt->bind_param('ssss', $name, $surname, $date, $address);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        return true;
    }
    public function addManager($name, $surname, $date, $PIVA, $email, $address, $pass)
    {
        $hash_pass = password_hash($pass, PASSWORD_DEFAULT);

        if (!$this->addPersona($name, $surname, $date, $address)) {
            return false;
        }
        $sql = "SELECT id FROM Anagrafica ORDER BY id DESC LIMIT 1";
        $result = $this->mysqli->query($sql);
        $row = $result->fetch_assoc();
        $stmt = $this->mysqli->prepare("INSERT INTO Organizzatori (id, email, password,partita_iva,id_anagrafica)
        VALUES (NULL, ?, ?, ?,?)");
        $stmt->bind_param('sssi', $email, $hash_pass, $PIVA, $row["id"]);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        return true;
    }

    public function updateUtente($id, $name, $surname, $date, $email, $address, $pass)
    {
        if (empty($pass) || !isset($pass)) { // in caso non si voglia modificare la password
            $sql = "UPDATE Utenti SET email = ? WHERE id = ? ";
            $stmt = $this->mysqli->prepare($sql);
            $stmt->bind_param('si', $email, $id);
            if (!$stmt->execute()) {
                trigger_error('Invalid query: ' . $this->mysqli->error);
                return false;
            }
        } else {
            $hash_pass = password_hash($pass, PASSWORD_DEFAULT);
            $sql = "UPDATE Utenti SET email = ?, password = ? WHERE id = ? ";
            $stmt = $this->mysqli->prepare($sql);
            $stmt->bind_param('ssi', $email, $hash_pass, $id);
            if (!$stmt->execute()) {
                trigger_error('Invalid query: ' . $this->mysqli->error);
                return false;
            }
        }
        $result = $this->mysqli->query($sql);
        if (!$result) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
        }
        $sql = "SELECT id_anagrafica FROM Utenti WHERE ? = id";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_anagrafica = $row["id_anagrafica"];
        $sql = "UPDATE Anagrafica SET nome = ?, cognome = ?, data_nascita = ?, indirizzo= ? WHERE id = ? ";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('ssssi', $name, $surname, $date, $address, $id_anagrafica);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        return $result;
    }

    public function updateOrganizzatore($id, $name, $surname, $date, $email, $address, $pass, $PIVA)
    {
        if (empty($pass) || !isset($pass)) {
            $sql = "UPDATE Organizzatori SET email = ?, partita_iva=? WHERE id = ? ";
            $stmt = $this->mysqli->prepare($sql);
            $stmt->bind_param('ssi', $email, $PIVA, $id);
            if (!$stmt->execute()) {
                trigger_error('Invalid query: ' . $this->mysqli->error);
                return false;
            }
        } else {
            $hash_pass = password_hash($pass, PASSWORD_DEFAULT);
            $sql = "UPDATE Organizzatori SET email = ?, password = ?, partita_iva= ? WHERE id = ? ";
            $stmt = $this->mysqli->prepare($sql);
            $stmt->bind_param('sssi', $email, $hash_pass, $PIVA, $id);
            if (!$stmt->execute()) {
                trigger_error('Invalid query: ' . $this->mysqli->error);
                return false;
            }
        }
        $sql = "SELECT id_anagrafica FROM Organizzatori WHERE ? = id";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_anagrafica = $row["id_anagrafica"];
        $sql = "UPDATE Anagrafica SET nome = ?, cognome = ?, data_nascita = ?, indirizzo=? WHERE id = ? ";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('ssssi', $name, $surname, $date, $address, $id_anagrafica);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        return $result;
    }


    public function addEvento(
        $nome,
        $prezzo,
        $descrizione,
        $data_inizio,
        $data_fine,
        $ora,
        $disponibilita,
        $nomeCategoria,
        $nomeFoto,
        $citta,
        $indirizzo
    ) {
        $this->addLuogo($indirizzo, $citta);
        //query per risalire agli id memorizzati
        $sql = "SELECT id FROM Categorie WHERE ? = nome";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $nomeCategoria);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_categoria = $row["id"];
        $sql = "SELECT id FROM Immagini WHERE ? = nome";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $nomeFoto);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_foto = $row["id"];
        $sql = "SELECT id FROM Luoghi WHERE ? = indirizzo";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $indirizzo);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_luogo = $row["id"];

        if (!($stmt = $this->mysqli->prepare("INSERT INTO Eventi (id, nome, descrizione,data_inizio,data_fine, ora, disponibilita, foto, luogo, categoria, prezzo )
        VALUES (NULL,?,?,?,?,?,?,?,?,?,?)"))) {
            echo "Prepare failed: (" .  $this->mysqli->errno . ") " .  $this->mysqli->error;
            return false;
        }
        if (
            !$stmt->bind_param(
                'sssssiiiid',
                $nome,
                $descrizione,
                $data_inizio,
                $data_fine,
                $ora,
                $disponibilita,
                $id_foto,
                $id_luogo,
                $id_categoria,
                $prezzo
            )
        ) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }


    public function addLuogo($indirizzo, $nomeCitta)
    {
        $stmt = $this->mysqli->prepare("INSERT INTO Luoghi (id,indirizzo, citta) VALUES (NULL,?,?)");
        $stmt->bind_param('ss', $indirizzo, $nomeCitta);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        return true;
    }
    public function addCategoria($nome)
    {
        $sql = "INSERT INTO Categorie (id, nome) VALUES (NULL, ?)";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $nome);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        return true;
    }

    public function getUserById($id)
    {

        $sql = "SELECT id_anagrafica FROM Utenti WHERE ? = id";
        $stmt = $this->mysqli->prepare($sql);
        $result = $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $row = ($stmt->get_result())->fetch_assoc();
        $id_an = $row["id_anagrafica"];
        $sql = "SELECT * FROM Utenti INNER JOIN Anagrafica ON Utenti.id_anagrafica= Anagrafica.id 
                                    WHERE Utenti.id_anagrafica= ?";
        $stmt = $this->mysqli->prepare($sql);
        $result = $stmt->bind_param('i', $id_an);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $row = ($stmt->get_result())->fetch_assoc();
        return $row;
    }
    public function getManagerById($id)
    {

        $sql = "SELECT id_anagrafica FROM Organizzatori WHERE ? = id";
        $stmt = $this->mysqli->prepare($sql);
        $result = $stmt->bind_param('i', $id);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $row = ($stmt->get_result())->fetch_assoc();
        $id_an = $row["id_anagrafica"];
        $sql = "SELECT * FROM Organizzatori INNER JOIN Anagrafica ON Organizzatori.id_anagrafica= Anagrafica.id 
                                    WHERE Organizzatori.id_anagrafica= ?";
        $stmt = $this->mysqli->prepare($sql);
        $result = $stmt->bind_param('i', $id_an);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $row = ($stmt->get_result())->fetch_assoc();
        return $row;
    }
    public function getIdUserByEmail($email)
    {
        $sql = "SELECT id FROM Utenti WHERE ? = email";
        $stmt = $this->mysqli->prepare($sql);
        $result = $stmt->bind_param('s', $email);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $row = ($stmt->get_result())->fetch_assoc();
        $id = $row["id"];
        return $id;
    }
    public function getIdManagerByEmail($email)
    {
        $sql = "SELECT id FROM Organizzatori WHERE ? = email";
        $stmt = $this->mysqli->prepare($sql);
        $result = $stmt->bind_param('s', $email);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        $row = ($stmt->get_result())->fetch_assoc();
        $id = $row["id"];
        return $id;
    }

    public function addOrdine($id_eventi, $id_utente)
    {
        $stmt = $this->mysqli->prepare("INSERT INTO Ordini (id, utente_id, data, ora) VALUES (NULL,?,?,?)");
        $time = date("H:i:s");
        $date = date("Y-m-d");
        $stmt->bind_param('iss', $id_utente, $date, $time);
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        $sql = "SELECT id FROM Ordini ORDER BY id DESC LIMIT 1";
        $result = $this->mysqli->query($sql);
        $row = $result->fetch_assoc();
        $id = $row["id"];
        $stmt = $this->mysqli->prepare("INSERT INTO Dettaglio_Ordine (id_ordine, id_evento,quantita) VALUES (?,?,?)");
        foreach ($id_eventi as $keys) {
            $stmt->bind_param('iii', $id, $keys['item_id'], $keys['item_quantity']);
            if (!$stmt->execute()) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            }
        }
    }

    public function editEvento(
        $id,
        $nome,
        $prezzo,
        $descrizione,
        $data_inizio,
        $data_fine,
        $ora,
        $disponibilita,
        $nomeCategoria,
        $citta,
        $indirizzo
    ) {
        $this->addLuogo($indirizzo, $citta);
        $sql = "SELECT id FROM Categorie WHERE ? = nome";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $nomeCategoria);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_categoria = $row["id"];

        $sql = "SELECT id FROM Luoghi WHERE ? = indirizzo";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param('s', $indirizzo);
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $id_luogo = $row["id"];

        $sql = "UPDATE Eventi SET nome = ?, descrizione = ?, data_inizio=?, data_fine=?,
                         ora=?, disponibilita=?, luogo=?, categoria=?, prezzo=? WHERE id = ? ";
        $stmt = $this->mysqli->prepare($sql);
        $stmt->bind_param(
            'sssssiiidi',
            $nome,
            $descrizione,
            $data_inizio,
            $data_fine,
            $ora,
            $disponibilita,
            $id_luogo,
            $id_categoria,
            $prezzo,
            $id
        );
        if (!$stmt->execute()) {
            trigger_error('Invalid query: ' . $this->mysqli->error);
            return false;
        }
        return true;
    }
}
