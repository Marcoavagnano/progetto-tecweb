<?php
session_start();
if (isset($_SESSION['LOGIN'])) :
  require("utils.php");
  include('header.php');
  include('utils.php');
?>
  <section class="section">
    <div class="container">
      <h1 class="text-center">Aggiungi Evento</h1>
      <hr>
      <form enctype="multipart/form-data" action="php/codiceEvento" method="post">
        <div class="form-group">
          <label> Nome </label>
          <input type="text" name="add_nome" class="form-control" required>
        </div>
        <div class="form-group">
          <label> Descrizione </label>
          <textarea name="add_descrizione" class="form-control" rows="5" required></textarea>
        </div>
        <div class="form-group">
          <label>Data Inizio</label>
          <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
            </div>
            <input class="form-control datepicker" placeholder="Select date" name="add_datainizio" type="text">
          </div>
        </div>
        <div class="form-group">
          <label>Data Fine</label>
          <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
            </div>
            <input class="form-control datepicker" placeholder="Select date" name="add_datafine" type="text">
          </div>
        </div>
        <div class="form-group">
          <label>Ora </label>
          <input type="time" name="add_ora" class="form-control" required>
        </div>
        <div class="form-group">
          <label> Prezzo </label>
          <input type="number" name="add_prezzo" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Disponibilità biglietti</label>
          <input type="number" name="add_disp" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Indirizzo</label>
          <input type="text" name="add_indirizzo" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Città</label>
          <input type="text" name="add_citta" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Categoria</label>
          <select class="browser-default custom-select" name="add_categoria">
            <?php
            $rows = $dbh->getCategorie();
            foreach ($rows as $row) { ?>
              <option value="<?php echo $row; ?>"><?php echo $row; ?></option>
            <?php
            }
            ?>
          </select>
        </div>
        <!-- <div class="custom-file" style="margin-bottom:20px;">
          <input name="img_evento" type="file" class="custom-file-input" id="validatedCustomFile" required>
          <label class="custom-file-label" for="validatedCustomFile">Scegli un file...</label>
        </div> -->
        <div class="form-group">
          <label for="exampleFormControlFile1">Scegli immagine</label>
          <input type="file" name="img_evento" class="form-control-file" required>
        </div>
        <a href="" class=" btn btn-danger">ANNULLA</a>
        <button name="btn_addEvento" class="btn btn-primary" type="submit">AGGIUNGI</button>
      </form>
    </div>
    </div>
  </section>
<?php
  include('footer.php');
else :
  // Redirect them to the login page
  header('Location: login.php');
  die();
endif;
?>