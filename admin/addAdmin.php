<?php
session_start();
require('../utils.php');

if (isset($_POST['registerbtn'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $cpassword = $_POST['confirmpassword'];

    if ($password === $cpassword) {

        $hash = password_hash($password, PASSWORD_DEFAULT);
        if ($dbh->addAdmin($username, $email, $hash)) {
            $_SESSION['success'] = "Profilo aggiunto correttamente";
            header('Location: admin.php');
        } else {
            $_SESSION['status'] = "Errore! Profilo non aggiunto";
            header('Location: admin.php');
        }
    } else {
        $_SESSION['status'] = "Le Password inserite non corrispondono";
        header('Location: admin.php');
    }
}
