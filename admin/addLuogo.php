<?php
session_start();
require("../utils.php");

if (isset($_POST['btn_addLuogo'])) {
   
    $indirizzo = $_POST['indirizzo'];
    $citta = $_POST['citta'];

    if ($dbh->addLuogo($indirizzo,$citta)) {
        $_SESSION['success'] = "Luogo Aggiunto";
        // header('Location: luoghi.php');
    } else {
        $_SESSION['status'] = "Luogo Non Aggiunto";
        //header('Location: luoghi.php');
    }
}

include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Aggiungi Luogo</h6>
        </div>
        <div class="card-body">
            <form action="#" method="post">     
                <div class="form-group">
                    <label> Indirizzo </label>
                    <input type="text" name="indirizzo" class="form-control" placeholder="Immetti Indirizzo" required>
                </div>
                <div class="form-group">
                    <label>Città</label>
                    <select name="citta" class="browser-default custom-select">
                        <?php
                        $rows = $dbh->getCitta();
                        foreach ($rows as $row) { ?>
                            <option value="<?php echo $row ?>"><?php echo $row ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <a href="luoghi.php" class="btn btn-danger">ANNULLA</a>
                <input type='hidden' name='edit_id' value="<?php echo $row['id'] ?>" />
                <button type='submit' name="btn_addLuogo" class="btn btn-primary">AGGIORNA</button>
            </form>
        </div>
    </div>
</div>
<?php
include('includes/scripts.php');
include('includes/footer.php');
?>