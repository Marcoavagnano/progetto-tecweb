<?php
session_start();
require("../utils.php");
include('includes/header.php');
include('includes/navbar.php');

$id_ordine = $_POST['edit_id'];
?>

<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Dettaglio Ordine</h6>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ID Evento</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Data Inizio</th>
                        <th scope="col">Data Fine</th>
                        <th scope="col">Prezzo</th>
                        <th scope="col">Quantità</th>
                    </tr>
                </thead>
                <?php
                foreach ($dbh->getDettaglioOrdineByIdOrdine($id_ordine) as $ordine) {
                    $evento = $dbh->getEventoById($ordine['id_evento']);
                ?>
                    <tr>
                        <td>
                            <a href="dettaglioEvento?edit_id=<?php echo $evento['id']; ?> ">
                                <?php echo $evento['id']; ?>
                            </a>
                        </td>
                        <td>
                            <?php echo $evento['nome']; ?>
                        </td>
                        <td>
                            <?php echo  date('d-m-Y', strtotime($evento['data_inizio'])) ?>
                        </td>
                        <td>
                            <?php echo  date('d-m-Y', strtotime($evento['data_fine'])); ?>
                        </td>
                        <td>
                            <?php echo $evento['prezzo'] . "€"; ?>
                        </td>
                        <td>
                            <?php echo $ordine['quantita']; ?>
                        </td>
                    </tr>
                <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>
</div>
<?php
include('includes/scripts.php');
include('includes/footer.php');
?>