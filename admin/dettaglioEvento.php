<?php
session_start();
require("../utils.php");
include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Dettaglio Evento</h6>
            <a class="btn btn-primary float-right hBack" href="addEvento.php" role="button">Indietro</a>
        </div>
        <div class="card-body">
            <?php
            $id = $_GET['edit_id'];
            $row = $dbh->getEventoById($id);
            ?>
            <form action="#" method="post">
                <div class="form-group">
                    <label> Nome </label>
                    <input type="text" name="add_nome" value="<?php echo $row['nome'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> Descrizione </label>
                    <textarea name="add_descrizione" class="form-control" rows="5" required><?php echo $row['descrizione'] ?></textarea>
                </div>
                <div class="form-group">
                    <label> Data Inizio </label>
                    <input type="date" name="add_datainizio" value="<?php echo $row['data_inizio'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> Data Fine </label>
                    <input type="date" name="add_datafine" value="<?php echo $row['data_fine'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Ora </label>
                    <input type="time" name="add_ora" value="<?php echo $row['ora'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> Prezzo </label>
                    <input type="number" name="add_prezzo" value="<?php echo $row['prezzo'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Disponibilità biglietti</label>
                    <input type="number" name="add_disp" value="<?php echo $row['disponibilita'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Indirizzo</label>
                    <input type="text" name="add_indirizzo" value="<?php echo ($dbh->getIndirizzoCitta($row['luogo']))[0]['indirizzo']; ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Città</label>
                    <input type="text" name="add_citta" value="<?php echo $dbh->getIndirizzoCitta($row['luogo'])[0]['citta']; ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Categoria</label>
                    <select class="browser-default custom-select" name="add_categoria">
                        <?php
                        $rows = $dbh->getCategorie();
                        foreach ($rows as $rowCat) { ?>
                            <option value="<?php echo $rowCat; ?>"><?php echo $rowCat; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Scegli immagine</label>
                    <input type="file" name="img_evento" class="form-control-file" required>
                    <img style="width: 300px; height: 200px;" src="<?php echo "../" . $dbh->getImgSrc($row['foto']); ?>" class="img-thumbnail" alt="Responsive image">
                </div>
                <a href="eventi" class="btn btn-danger">ANNULLA</a>
                <input type='hidden' name='edit_id' value="<?php echo $row['id'] ?>" />
                <button name="btn_aggiorna" class="btn btn-primary">AGGIORNA</button>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>