<?php
require("../utils.php");
session_start();
if (isset($_POST['btn_aggiorna'])) {
    $id = $_POST['edit_id'];
    $nome = $_POST['nome'];
    $cognome = $_POST['cognome'];
    $data = $_POST['data_nascita'];
    $email = $_POST['email'];
    $indirizzo = $_POST['indirizzo'];
    $password = $_POST['password'];

    $hash = password_hash($password, PASSWORD_DEFAULT);
    if ($dbh->updateUtente($id, $nome, $cognome, $data, $email, $indirizzo, $hash)) {
        $_SESSION['success'] = "Profilo modificato correttamente";
        header('Location: utenti.php');
    } else {
        $_SESSION['status'] = "Errore! Profilo non aggiunto";
        header('Location: utenti.php');
    }
}

if (isset($_POST['delete_btn'])) {
    $id = $_POST['edit_id'];
    if ($dbh->deleteUtente($id)) {
        $_SESSION['success'] = "Profilo eliminato correttamente";
        header('Location: utenti.php');
    } else {
        $_SESSION['status'] = "Errore! Profilo non eliminato";
        header('Location: utenti.php');
    }
}

include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Modifica Profilo Utente</h6>
        </div>
        <div class="card-body">

            <?php
            if (isset($_POST['edit_btn'])) {
                $id = $_POST['edit_id'];
                $rows = $dbh->getProfileById($id);
                foreach ($rows as $row) {
                    ?>
                    <form action="#" method="POST">
                        <div class="form-group">
                            <label> Nome </label>
                            <input type="text" name="nome" class="form-control" value="<?php echo $row['nome'] ?>" placeholder="Immetti Nome" required>
                        </div>
                        <div class="form-group">
                            <label> Cognome </label>
                            <input type="text" name="cognome" class="form-control" value="<?php echo $row['cognome'] ?>" placeholder="Immetti Cognome" required>
                        </div>
                        <div class="form-group">
                            <label> Data di nascita </label>
                            <input type="date" name="data_nascita" class="form-control" value="<?php echo $row['data_nascita'] ?>" placeholder="Immetti Data di Nascita" required>
                        </div>
                        <div class="form-group">
                            <label> Email </label>
                            <input type="text" name="email" class="form-control" value="<?php echo $row['email'] ?>" placeholder="Immetti Email" required>
                        </div>
                        <div class="form-group">
                            <label> Indirizzo </label>
                            <input type="text" name="indirizzo" class="form-control" value="<?php echo $row['indirizzo'] ?>" placeholder="Immetti Indirizzo" required>
                        </div>
                        <div class="form-group">
                            <label> Password </label>
                            <input type="password" name="password" class="form-control" value="<?php echo $row['password'] ?>" placeholder="Immetti Password" required>
                        </div>
                        <a href="utenti.php" class="btn btn-danger">ANNULLA</a>
                        <input type='hidden' name='edit_id' value="<?php echo $id ?>">
                        <button class="btn btn-primary" name="btn_aggiorna" href="#">AGGIORNA</button>
                    </form>
            <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<?php
include('includes/scripts.php');
include('includes/footer.php');
?>