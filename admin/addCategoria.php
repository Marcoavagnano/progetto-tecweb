<?php
session_start();
require('../utils.php');

if (isset($_POST['registercategoriabtn'])) {
    $nome = $_POST['nome'];
    $nome = ucwords(strtolower($nome));
    if (!empty($nome) && $dbh->addCategoria($nome)) {
        $_SESSION['success'] = "Categoria Aggiunta";
        header('Location: categorie.php');
    } else {
        $_SESSION['status'] = "Errore! Categoria Non Aggiunta";
        header('Location: categorie.php');
    }
}

if (isset($_POST['eliminaCategoria'])) {
    $nome = $_POST['nome'];
    if ($dbh->deleteCategoria($nome)) {
        $_SESSION['success'] = "Categoria Eliminata";
        header('Location: categorie.php');
    } else {
        $_SESSION['status'] = "Categoria Non Eliminata";
        header('Location: categorie.php');
    }
}
