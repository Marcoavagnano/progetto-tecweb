<?php
session_start();
if (isset($_SESSION['admin_id'])) :
    require("../utils.php");
    include('includes/header.php');
    include('includes/navbar.php');
?>

    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary">Lista Eventi</h2>
                <a class="btn btn-primary float-right" href="addEvento.php" role="button">Aggiungi Evento</a>
            </div>

            <div class="card-body">
                <?php
                if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                    echo "<div class='alert alert-success' role='alert'>" . $_SESSION['success'] . "</div>";
                    unset($_SESSION['success']);
                }
                if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                    echo "<div class='alert alert-danger' role='alert'>" . $_SESSION['status'] . "</div>";
                    unset($_SESSION['status']);
                }
                ?>
                <div class="table-responsive">
                    <?php
                    $dbh->getEventiTable();
                    ?>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

<?php
    include('includes/scripts.php');
    include('includes/footer.php');
else :
    // Redirect them to the login page
    header('Location: login.php');
    die();
endif;
?>