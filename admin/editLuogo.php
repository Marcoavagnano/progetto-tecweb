<?php
session_start();
require("../utils.php");
if (isset($_POST['edit_btn'])) :
    include('includes/header.php');
    include('includes/navbar.php');
?>

    <div class="container-fluid">

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"> Modifica Profilo Admin</h6>
            </div>
            <div class="card-body">

                <?php
                if (isset($_POST['edit_btn'])) {
                    $id = $_POST['edit_id'];
                    $rows = $dbh->getLuogoById($id);
                    foreach ($rows as $row) {
                ?>
                        <form action="#" method="post">
                            <div class="form-group">
                                <label> Nome </label>
                                <input type="text" name="edit_nome" class="form-control" value="<?php echo $row['nome'] ?>" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <label> Numero Posti </label>
                                <input type="text" name="edit_posti" class="form-control" value="<?php echo $row['numero_posti'] ?>" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <label> Indirizzo </label>
                                <input type="text" name="edit_indirizzo" class="form-control" value="<?php echo $row['indirizzo'] ?>" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <label>Telefono</label>
                                <input type="email" name="edit_telefono" class="form-control" value="<?php echo $row['telefono'] ?>" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label>Città</label>
                                <select class="browser-default custom-select">
                                    <?php
                                    $rows = $dbh->getCitta();
                                    foreach ($rows as $row) {
                                        echo "<option value='$row'>" . $row . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <a href="luoghi.php" class="btn btn-danger">ANNULLA</a>
                            <input type='hidden' name='edit_id' value="<?php echo $row['id'] ?>" />
                            <button name="btn_aggiorna" class="btn btn-primary">AGGIORNA</button>
                        </form>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
<?php
    include('includes/scripts.php');
    include('includes/footer.php');
elseif (isset($_POST['delete_btn'])) :
    $id = $_POST['edit_id'];
    if ($dbh->deleteLuogoById($id)) {
        $_SESSION['success'] = "Luogo eliminato correttamente";
    } else
        $_SESSION['status'] = "Errore! Luogo non eliminato";
    header('location: luoghi');
elseif (isset($_POST['btn_aggiorna'])) :
    echo "aggiorna";
    $nome = $_POST['edit_nome'];
    $posti = $_POST['edit_posti'];
    $telefono = $_POST['edit_telefono'];
    $citta = $_POST['edit_citta'];
    if ($dbh->updateAdmin($id, $username, $email, $password))
        $_SESSION['success'] = "Luogo aggiornato correttamente";
    else
        $_SESSION['success'] = "Errore! Luogo non aggiornato";
    header('location: luoghi');
endif;
?>