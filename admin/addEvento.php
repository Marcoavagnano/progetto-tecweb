<?php
session_start();
require("../utils.php");
include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Aggiungi Evento</h6>
        </div>
        <div class="card-body">
            <form enctype="multipart/form-data" action="codiceEvento.php" method="post">
                <div class="form-group">
                    <label> Nome </label>
                    <input type="text" name="add_nome" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> Descrizione </label>
                    <textarea name="add_descrizione" class="form-control" rows="5" required></textarea>
                </div>
                <div class="form-group">
                    <label> Data Inizio </label>
                    <input type="date" name="add_datainizio" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> Data Fine </label>
                    <input type="date" name="add_datafine" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Ora </label>
                    <input type="time" name="add_ora" class="form-control" required>
                </div>
                <div class="form-group">
                    <label> Prezzo </label>
                    <input type="number" name="add_prezzo" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Disponibilità biglietti</label>
                    <input type="number" name="add_disp" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Luogo</label>
                    <select class="browser-default custom-select" name="add_luogo">
                        <?php
                        $rows = $dbh->getLuoghi();
                        foreach ($rows as $row) { ?>
                            <option value="<?php echo $row; ?>"><?php echo $row; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Categoria</label>
                    <select class="browser-default custom-select" name="add_categoria">
                        <?php
                        $rows = $dbh->getCategorie();
                        foreach ($rows as $row) { ?>
                            <option value="<?php echo $row; ?>"><?php echo $row; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlFile1">Scegli immagine</label>
                    <input type="file" name="img_evento" class="form-control-file" required>
                </div>
                <a href="luoghi.php" class="btn btn-danger">ANNULLA</a>
                <button name="btn_addEvento" class="btn btn-primary">AGGIUNGI</button>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
include('../utility/utils.php');
?>