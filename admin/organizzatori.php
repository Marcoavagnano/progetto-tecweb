<?php
session_start();
if (isset($_SESSION['admin_id'])) :
    require("../utils.php");
    include('includes/header.php');
    include('includes/navbar.php');
?>


    <div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Aggiungi Dati</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="modifyUtente.php" method="POST">

                    <div class="modal-body">

                        <div class="form-group">
                            <label> Nome </label>
                            <input type="text" name="nome" class="form-control" placeholder="Inserisci Nome" required>
                        </div>
                        <div class="form-group">
                            <label> Cognome </label>
                            <input type="text" name="cognome" class="form-control" placeholder="Inserisci Cognome" required>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Inserisci Email" required>
                        </div>
                        <div class="form-group">
                            <label> Indirizzo </label>
                            <input type="text" name="indirizzo" class="form-control" placeholder="Inserisci Indirizzo" required>
                        </div>
                        <div class="form-group">
                            <label> Data di Nascita </label>
                            <input type="text" name="data_nascita" class="form-control" placeholder="Inserisci Data di Nascita" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Inserisci Password" required>
                        </div>
                        <div class="form-group">
                            <label>Conferma Password</label>
                            <input type="password" name="confirmpassword" class="form-control" placeholder="Conferma Password" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
                        <button type="submit" name="registerbtn" class="btn btn-primary">Salva</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary">Profili Organizzatori</h2>
                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#addadminprofile">
                    Aggiungi Organizzatore
                </button>
            </div>

            <div class="card-body">
                <?php
                if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                    echo "<div class='alert alert-success' role='alert'>" . $_SESSION['success'] . "</div>";
                    unset($_SESSION['success']);
                }
                if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                    echo "<div class='alert alert-danger' role='alert'>" . $_SESSION['status'] . "</div>";
                    unset($_SESSION['status']);
                }
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="utentiTable" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Cognome</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Indirizzo</th>
                                <th>Data di Nascita</th>
                                <th>Modifica</th>
                                <th>Elimina</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $dbh->getOrgTable();
                            ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

<?php
    include('includes/scripts.php');
    include('includes/footer.php');
else :
    // Redirect them to the login page
    header('Location: login.php');
    die();
endif;
?>