<?php
include('../utility/utils.php');
session_start();
if (isset($_POST['btn_addEvento'])) {
    $nome = $_POST['add_nome'];
    $descrizione = $_POST['add_descrizione'];
    $data_inizio = $_POST['add_datainizio'];
    $data_fine = $_POST['add_datafine'];
    $ora = $_POST['add_ora'];
    $ora = date("H:i", strtotime($ora));
    $prezzo = $_POST['add_prezzo'];
    $disponibilita = $_POST['add_disp'];
    $categoria = $_POST['add_categoria'];
    $nomeLuogo = $_POST['add_luogo'];
    if (isset($_FILES['img_evento']) || !is_uploaded_file($_FILES['img_evento']['tmp_name'])) {
        if (addImage('img_evento')) {
            $nomeFoto = $_FILES['img_evento']['name'];
            echo $nomeLuogo;
        }
    }
    if ($dbh->addEvento(
        $nome,
        $prezzo,
        $descrizione,
        $data_inizio,
        $data_fine,
        $ora,
        $disponibilita,
        $categoria,
        $nomeFoto,
        $nomeLuogo
    )) {
        $_SESSION['success'] = "Evento aggiunto correttamente";
        //header('Location: eventi');
    } else {
        $_SESSION['status'] = "Errore! Evento non aggiunto";
        //header('Location: eventi');
    }
}
