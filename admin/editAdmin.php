<?php
session_start();
require("../utils.php");
if (isset($_POST['edit_btn'])) :
    include('includes/header.php');
    include('includes/navbar.php');
?>

    <div class="container-fluid">

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"> Modifica Profilo Admin</h6>
            </div>
            <div class="card-body">

                <?php
                if (isset($_POST['edit_btn'])) {
                    $id = $_POST['edit_id'];
                    $rows = $dbh->getAdminById($id);
                    foreach ($rows as $row) {
                ?>
                        <form action="#" method="post">
                            <div class="form-group">
                                <label> Username </label>
                                <input type="text" name="edit_username" class="form-control" value="<?php echo $row['username'] ?>" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="edit_email" class="form-control" value="<?php echo $row['email'] ?>" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="edit_password" class="form-control" value="<?php echo $row['password'] ?>" placeholder="Password" required>
                            </div>
                            <a href="admin.php" class="btn btn-danger">ANNULLA</a>
                            <input type='hidden' name='edit_id' value="<?php echo $row['id'] ?>" />
                            <button name="btn_aggiorna" class="btn btn-primary">AGGIORNA</button>
                        </form>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
<?php
    include('includes/scripts.php');
    include('includes/footer.php');
elseif (isset($_POST['delete_btn'])) :
    $id = $_POST['edit_id'];
    if ($dbh->deleteAdminById($id)) {
        $_SESSION['success'] = "Profilo eliminato correttamente";
    } else
        $_SESSION['status'] = "Errore! Profilo non eliminato";
    header('location: admin.php');
elseif (isset($_POST['btn_aggiorna'])) :
    $id = $_POST['edit_id'];
    $username = $_POST['edit_username'];
    $email = $_POST['edit_email'];
    $password = $_POST['edit_password'];
    if ($dbh->updateAdmin($id, $username, $email, $password))
        $_SESSION['success'] = "Profilo aggiornato correttamente";
    else
        $_SESSION['success'] = "Errore! Profilo non aggiornato";
    header('location: admin.php');
endif;
?>