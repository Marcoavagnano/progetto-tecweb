<?php
session_start();
include('../utils.php');
if (!empty($_POST)) {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        // Getting submitted user data from database
        $username =  $_POST['username'];
        $user = $dbh->getAdminByUsername($username);

        // Verify user password and set $_SESSION
        if (password_verify($_POST['password'], $user['password'])) {
            echo $user['id'];
            $_SESSION['admin_id'] = $user['id'];
            header("Location: dashboard.php");
        } else {
            $_SESSION["adminLogin_error"] = "Username o password incorretti";
            header("Location: login.php");
        }
    }
}
