<?php
session_start();
if (isset($_SESSION['admin_id'])) {
  include('includes/header.php');
  include('includes/navbar.php');
  
  include('includes/scripts.php');
  include('includes/footer.php');

 } 
else {
  // Redirect them to the login page
  header('Location: login.php');
  die();
}
