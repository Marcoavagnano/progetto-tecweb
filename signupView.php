<?php
if (!isset($_SESSION))
    session_start();
include('header.php');
?>
<section class="section">
    <div class="container-fluid">

        <div class="row justify-content-center">

            <div class="col-lg-5">
                <h4 class="text-center">Registrazione</h4>

                <?php
                if (isset($_SESSION['errSignup'])) {
                    echo   '<div class="alert alert-danger m-4 text-center" role="alert">';
                    echo $_SESSION['errSignup'];
                    echo "</div>";
                    unset($_SESSION['errSignup']);
                }
                ?>
                <div class="card-body px-lg-5 py-lg-5">
                    <form class="text-center" style="color: #757575;" action="php/signup.php" method="post">
                        <div class="form-group">
                            <label for="regname">Nome</label>
                            <input type="text" id="regname" name="regname" class="form-control" required>
                        </div>
                        <div class="form-group"">
                    <label for=" regsurname">Cognome</label>
                            <input type="text" id="regsurname" name="regsurname" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="regemail">E-mail</label>
                            <input type="email" id="regemail" name="regemail" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for=" regaddress">Indirizzo</label>
                            <input type="text" class="form-control" name="regaddress" id="regaddress" required>
                        </div>
                        <div class="form-group">
                            <label for="regdate">Data di nascita</label>
                            <input type="date" class="form-control text-center" name="regdate" id="regdate" required>
                        </div>
                        <?php
                        if (isset($_SESSION["MANAGER"])) {
                            echo '
            
                        <div class="form-group">
                        <label for="regdate">Partita IVA</label>
                        <input type="text" class="form-control" name="regiva" id="regiva">  
                        </div>';
                        }
                        ?>
                        <div class="form-group">
                            <label for="regpass">Password</label>
                            <input type="password" required id="regpass" class="form-control" name="regpass" aria-describedby="materialRegisterFormPasswordHelpBlock">
                        </div>
                        <div class="form-group"">
                    <label for=" regconfpass">Ripeti password</label>
                            <input type="password" required id="regconfpass" name="regconfpass" class="form-control" aria-describedby="materialRegisterFormPasswordHelpBlock">
                        </div>
                        <button class="btn btn-primary btn-block " type="submit">Registrati al sito</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include('footer.php');
?>