<?php
include('header.php');
include('utils.php');
?>
<main>
    <section>
        <div class="container" id="homeContainer">
        <h3 class="text-center mt-5 mb-3">Eventi di Tendenza</h3>
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <?php
                    $topEventi = $dbh->getTopEventi();
                    $count = 0;
                    foreach ($topEventi as $row) {
                        if ($count == 0) {
                    ?>
                            <div class="carousel-item active">
                                <a href="dettaglioEvento?id_evento=<?php echo $row['id'] ?>">
                                    <img class="d-block w-100" src="<?php echo $dbh->getImgSrc($row['foto']); ?>" alt="First slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5 id="eventoCarosello"><?php echo $row['nome']; ?></h5>
                                </a>
                            </div>
                </div>
            <?php
                        } else { ?>
                <div class=" carousel-item">
                    <a href="dettaglioEvento?id_evento=<?php echo $row['id'] ?>">
                        <img class="d-block w-100" src="<?php echo $dbh->getImgSrc($row['foto']); ?>" alt="First slide">
                        <div class="carousel-caption d-none d-md-block">
                            <h5 id="eventoCarosello"><?php echo $row['nome']; ?></h5>
                    </a>
                </div>
            </div>
    <?php
                        }
                        $count++;
                    }
    ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>

        <div class="row">
            <div class="col col-md-8 offset-md-2">
            <h3 class="text-center mt-5 mb-3">Eventi Imminenti</h3>
                <hr class="sidebar-divider">
            </div>
        </div>

        <div class="row">
            <?php
            $randomEventi = $dbh->getRandomEventi();
            foreach ($randomEventi as $row) {
                $luogo =   $dbh->getIndirizzoCitta($row['luogo']);

            ?>
                <div class="col col-lg-6 col-md-6 col-12">

                    <div class="container">
                        <div class="card card-flip h-100">
                            <div class="card-front ">
                                <img src="<?php echo $dbh->getImgSrc($row['foto']); ?>" class="img-thumbnail tumb" alt="Avatar" style="max-width:100%;">
                            </div>
                            <div class="card-back bg-light">
                                <div class="card-body">
                                    <h5 class="card-title">Quando:</h5>
                                    <h6 class="card-text">Data Inizio: <?php echo  date('d-m-Y', strtotime($row['data_inizio'])); ?></h6>
                                    <h6 class="card-text">Data Fine: <?php echo  date('d-m-Y', strtotime($row['data_fine'])); ?></h6>
                                    <h5 class="card-title">Dove:</h5>
                                    <h6 class="card-text"> <?php echo $luogo[0]['indirizzo'] . ', ' . $luogo[0]['citta']; ?></h6>
                                </div>
                            </div>
                        </div>
                        <h6><?php echo $row['nome'] ?></h6>
                        <form action="dettaglioEvento.php" method="GET">
                            <input type='hidden' name='id_evento' value='<?php echo $row['id'] ?>' ">
                                <p><button class=" btn btn-primary">Vai all' Evento &raquo;</button></p>
                        </form>

                    </div>
                </div>
            <?php } ?>
        </div>
        </div>

        </div>
    </section>
</main>

<?php
include('footer.php');
?>