<?php
session_start();
if (isset($_SESSION['LOGIN'])) :
    require("utils.php");
    include('header.php');
    include('utils.php');

    $idEvento = $_GET['idEvento'];
    $evento = $dbh->getEventoById($idEvento);
?>
    <section class="section">
        <div class="container">
            <h1 class="text-center">Modifica Evento</h1>
            <hr>
            <form enctype="multipart/form-data" action="php/codiceEvento" method="post">
                <div class="form-group">
                    <label> Nome </label>
                    <input type="text" name="add_nome" class="form-control" required value="<?php echo $evento['nome']; ?>">
                </div>
                <div class="form-group">
                    <label> Descrizione </label>
                    <textarea name="add_descrizione" class="form-control" rows="5" required><?php echo $evento['descrizione']; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Data Inizio</label>
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                        </div>
                        <input class="form-control datepicker" placeholder="Select date" type="text" value="<?php echo date('d-m-Y', strtotime($evento['data_inizio'])); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Data Fine</label>
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                        </div>
                        <input class="form-control datepicker" placeholder="Select date" type="text" value="<?php echo date('d-m-Y', strtotime($evento['data_fine'])); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Ora </label>
                    <input type="time" name="add_ora" class="form-control" value="<?php echo $evento['ora']; ?>" required>
                </div>
                <div class="form-group">
                    <label> Prezzo </label>
                    <input type="number" name="add_prezzo" class="form-control" value="<?php echo $evento['prezzo']; ?>" required>
                </div>
                <div class="form-group">
                    <label>Disponibilità biglietti</label>
                    <input type="number" name="add_disp" class="form-control" value="<?php echo $evento['disponibilita']; ?>" required>
                </div>
                <div class="form-group">
                    <label>Indirizzo</label>
                    <input type="text" name="add_indirizzo" value="<?php echo ($dbh->getIndirizzoCitta($evento['luogo']))[0]['indirizzo'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Città</label>
                    <input type="text" name="add_citta" value="<?php echo ($dbh->getIndirizzoCitta($evento['luogo']))[0]['citta'] ?>" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Categoria</label>
                    <select class="browser-default custom-select" name="add_categoria">
                        <?php
                        $rows = $dbh->getCategorie();
                        $cat = $dbh->getCategoriaById($evento['categoria']);
                        foreach ($rows as $row) {
                            if ($row == $cat) {
                        ?>
                                <option value="<?php echo $row; ?>" selected><?php echo $row; ?></option>
                            <?php
                            } else {
                            ?>
                                <option value="<?php echo $row; ?>"><?php echo $row; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <!-- <label>Scegli Immagine</label>
                <div class="custom-file" style="margin-bottom:20px;">
                    <input type="file" class="custom-file-input" id="validatedCustomFile">
                    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                    <div class="invalid-feedback">Example invalid custom file feedback</div>
                </div> -->
                <a href="eventiOrganizzati" class=" btn btn-danger">ANNULLA</a>
                <input type="hidden" name="input_id" value="<?php echo $evento['id']; ?>">
                <button name="btn_editEvento" class="btn btn-primary" type="submit">AGGIORNA</button>
            </form>
        </div>
        </div>
    </section>
<?php
    include('footer.php');
else :
    // Redirect them to the login page
    header('Location: login.php');
    die();
endif;
?>