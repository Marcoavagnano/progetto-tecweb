<?php
session_start();
if (isset($_SESSION['LOGIN'])) :
    require("utils.php");
    include('header.php');
    include('utils.php');
?>
    <section class="section">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <h1>Lista Ordini</h1>
            </div>
            <div class="table-responsive">
                <?php
                $dbh->getOrdiniTableByUtente($_SESSION['LOGIN']);
                ?>
            </div>
        </div>
    </section>
<?php
    include('footer.php');
else :
    // Redirect them to the login page
    header('Location: login.php');
    die();
endif;
?>