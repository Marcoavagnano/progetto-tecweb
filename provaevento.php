<?php
session_start();
if (isset($_SESSION['LOGIN'])) :
    require("utils.php");
    include('header.php');
    include('utils.php');
?>
    <section class="section">
        <div class="container">

            <!-- For demo purpose -->
            <div class="row text-center text-white mb-5">
                <div class="col-lg-7 mx-auto">
                    <h1 class="display-4">Eventi Organizzati</h1>
                </div>
            </div>
            <!-- End -->

            <div class="row">
                <div class="col-lg-8 mx-auto">

                    <!-- List group-->
                    <ul class="list-group shadow">
                        <?php
                        $id_organizzatore = $_SESSION['LOGIN'];
                        foreach ($dbh->getEventiByOrganizzatore($id_organizzatore) as $eventi) {
                            $evento = $dbh->getEventoById($eventi['id_evento']);
                        ?>
                            <!-- list group item-->
                            <li class="list-group-item">
                                <!-- Custom content-->
                                <div class="media align-items-lg-center flex-column flex-lg-row p-3">
                                    <div class="media-body order-2 order-lg-1">

                                        <h5 class="mt-0 font-weight-bold mb-2"><?php echo $evento['nome']; ?></h5>
                                        <p class="font-italic text-muted mb-0 small"><?php echo $evento['descrizione']; ?></p>
                                        <div class="d-flex align-items-center justify-content-between mt-1">
                                            <h6 class="font-weight-bold my-2"><?php echo $evento['prezzo'] . "€"; ?></h6>
                                            <ul class="list-inline small">
                                                <li class="list-inline-item m-0"><?php echo "Biglietti Acquistati: " . $dbh->contaEventiCompratiInTotale($evento['id']) ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <a href="modificaEvento?idEvento=<?php echo $evento['id']; ?> "><span><i class="fa fa-pencil alt"></i> Modifica</span></a>
                                    <img src=" <?php echo $dbh->getImgSrc($evento['foto']) ?>" alt="Immagine Evento" width="200" class="ml-lg-5 order-1 order-lg-2">
                                </div>
                                <!-- End -->
                            </li>

                        <?php
                        }
                        ?>
                        <!-- End -->
                    </ul>
                    <!-- End -->
                </div>
            </div>
        </div>
    </section>

<?php
    include('footer.php');
else :
    // Redirect them to the login page
    header('Location: login.php');
    die();
endif;
?>

<!-- <div class="row">
                        <div class="col col-md-3">

                        </div>
                    </div> -->