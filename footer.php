<footer class="footer">
  <div class="container">
    <div class="row row-grid align-items-center mb-5">
      <div class="col-lg-6">
        <h3 class="text-primary font-weight-light mb-2">Tecnologie Web</h3>
        <h4 class="mb-0 font-weight-light">Simone Fontanella - Marco Avagnano</h4>
      </div>
    </div>
    <hr>
    <div class="row align-items-center justify-content-md-between">
      <div class="col-md-6">
        <div class="copyright">
          © 2019 <a href="https://www.unibo.it/it/campus-cesena" target="_blank">UNIBO</a>.
        </div>
      </div>
      <div class="col-md-6">
        <ul class="nav nav-footer justify-content-end">
          <li class="nav-item">
            <a href="https://www.unibo.it/it/campus-cesena" class="nav-link" target="_blank">Unibo</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link" target="_blank">Contatti</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<!-- Core -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/popper/popper.min.js"></script>
<script src="assets/vendor/bootstrap/bootstrap.min.js"></script>
<script src="assets/vendor/headroom/headroom.min.js"></script>
<!-- Optional JS -->
<script src="assets/vendor/onscreen/onscreen.min.js"></script>
<script src="assets/vendor/nouislider/js/nouislider.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- Argon JS -->
<script src="assets/js/argon.js?v=1.1.0"></script>
<script src="js/userdata.js" type="text/javascript"></script>
<script src="js/cart.js" type="text/javascript"></script>
<script src="js/notification.js" type="text/javascript"></script>
<!-- Google Maps API -->
<script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap" async defer></script>
<script src="js/googleMaps.js" type="text/javascript"></script>
<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="js/helper.js" type="text/javascript"></script>
</script>
</body>

</html>