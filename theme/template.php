<!DOCTYPE html>
<html lang="it">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Design System for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Argon Design System - Free Design System for Bootstrap 4</title>
    <!-- Favicon -->
    <link href="./assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="./argon/assets/vendor/nucleo/css/nucleo.css" rel="stylesheet">
    <link href="./argon/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="./argon/assets/css/argon.css?v=1.1.0" rel="stylesheet">
</head>

<body>
    <header class="header-global">
        <nav class="navbar navbar-expand-lg navbar-dark bg-default">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <h3 style="color:white" class="display-4">Eventi</h3>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar-default">
                    <div class="navbar-collapse-header">
                        <div class="row">
                            <div class="col-6 collapse-brand">
                                <h3>Eventi</h3>
                            </div>
                            <div class="col-6 collapse-close">
                                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-default" aria-controls="navbar-default" aria-expanded="false" aria-label="Toggle navigation">
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link" data-toggle="dropdown" role="button">
                                <i class="ni ni-shop d-lg-none"></i>
                                <span class="nav-link-inner--text">Home</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link" data-toggle="dropdown" role="button">
                                <i class="ni ni-collection d-lg-none"></i>
                                <span class="nav-link-inner--text">Eventi</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="nav-link" data-toggle="dropdown" role="button">
                                <i class="ni ni-bullet-list-67 d-lg-none"></i>
                                <span class="nav-link-inner--text">Categorie</span>
                            </a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-lg-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link nav-link-icon" href="#" id="navbar-default_dropdown_1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ni ni-circle-08"></i>
                                <span class="nav-link-inner--text d-lg-none">Account</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
                                <a class="dropdown-item" data-toggle="modal" href="#logIn">Accedi</a>
                                <a class="dropdown-item" href="#">Il mio Profilo</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Registrati</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-icon" href="carrello.php">
                                <i class="ni ni-cart"></i>
                                <span class="nav-link-inner--text d-lg-none">Carrello</span>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            <div id="logIn" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Accedi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="php/login.php" method="post">
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input type="email" class="form-control" name="inputemail" aria-describedby="emailHelp" placeholder="Inserisci l'email">
                                </div>
                                <div class="form-group">
                                    <label for="inputPass">Password</label>
                                    <input type="password" class="form-control" name="inputpass" placeholder="Password">
                                </div>
                                <a href="signup.php">Non sei ancora registrato? Registrati.</a><br><br>
                                <button type="submit" class="btn btn-primary">Accedi</button>
                                <?php

                                if (isset($_SESSION['loginState'])) {
                                    echo   "<div class=\"alert alert-danger\" role=\"alert\">";
                                    echo $_SESSION['loginState'];
                                    echo "</div>";
                                    unset($_SESSION['loginState']);
                                    session_abort();
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="signIn" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Registrati</h5>
                            <button type="submit" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="php/signIn.php" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputName">Nome</label>
                                            <input type="text" class="form-control" name="regname" placeholder="Nome">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputSurname">Cognome</label>
                                            <input type="text" class="form-control" name="regsurname" placeholder="Cognome">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail">Email</label>
                                            <input type="email" class="form-control" name="regemail" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputAddress">Indirizzo</label>
                                            <input type="text" class="form-control" name="regaddress" placeholder="Indirizzo">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputDate">Data di nascita</label>
                                            <input type="date" class="form-control" name="regdate" placeholder="Data di nascita">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputPass">Password</label>
                                            <input type="password" class="form-control" name="regpass" placeholder="Password">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Registrati</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <!-- Core -->
    <script src="./argon/assets/vendor/jquery/jquery.min.js"></script>
    <script src="./argon/assets/vendor/popper/popper.min.js"></script>
    <script src="./argon/assets/vendor/bootstrap/bootstrap.min.js"></script>
    <script src="./argon/assets/vendor/headroom/headroom.min.js"></script>
    <!-- Optional JS -->
    <script src="./argon/assets/vendor/onscreen/onscreen.min.js"></script>
    <script src="./argon/assets/vendor/nouislider/js/nouislider.min.js"></script>
    <script src="./argon/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <!-- Argon JS -->
    <script src="./argon/assets/js/argon.js?v=1.1.0"></script>
</body>

</html>