
<?php
ob_start();
include('utils.php');
include('header.php');
if(isset($_COOKIE["LOGIN"])){
    $id=$_COOKIE["LOGIN"];
}else{
    if(isset($_SESSION["LOGIN"]))
    $id=$_SESSION["LOGIN"];
    else{
        header("Location: login");
        exit();
    }
}

$index=0;
$inputName=array(
    0 => "regemail",
    1 => "regpass",
    2 => "regiva",
    3 => "regname",
    4 => "regsurname",
    5 => "regdate",
    6 => "regaddress"
);
echo '<div class="container-fluid">
<div class="row justify-content-center">
<div class="col-lg-5">
  
    <div class="card-body px-lg-5 py-lg-5">
      <div class="text-center mb-4">';
      if(isset($_SESSION["MANAGER"])){
        $userInfo=$dbh->getManagerById($id);  
        echo '<h4>Profilo organizzatore</h4>';  
      }
      else {
        $userInfo=$dbh->getUserById($id);
        array_splice($inputName, 2, 1);
        echo '<h4>Profilo utente</h4>';
      }
 echo' </div>
        <form action="php/update" method="post">';
foreach($userInfo as $key => $value):
    $key=strtoupper($key);
    if($key=="DATA_NASCITA")
        $type="date";
    else
        if(($key=="PASSWORD")){
            $type="password";
            $value="";
        }
        else
            $type="text";

    if($key != "ID" && $key != "ID_ANAGRAFICA" ): //non stampiamo gli id perchè non sono modificabili
            echo '
                  <div class="form-group">
                    <label><h6>' . $key . '</h6></label>
                      <input class="form-control inputData" name="'.$inputName[$index].'" value="' . $value . '" disabled type="'.$type.'">
                  </div>
                  ';
                  if($key=="PASSWORD"):
                    echo '
                    <div class="form-group">
                    <label><h6>CONFERMA PASSWORD</h6></label>
                      <input class="form-control inputData" name="regconfpass" value="" disabled type="password">
                  </div>
                    ';
                  endif;
                 
                  $index++;
    endif;
endforeach;
echo '  
<div class="row">
<div class="col-sm">
<button class="btn btn-default btn-block modify">Modifica i dati</button>

</div>
<div class="col-sm">
<style>
@media (max-width: 575px) { 
    .sub{
        margin-top: 10px;
    }
}
</style>
<button class="btn  btn-default  btn-block sub" type="submit" disabled>Invia</button>
</div>
</div>
';
if (isset($_SESSION['errUpdate'])){
    echo   '<div class="alert alert-danger" role="alert" style="margin-top: 10px;">';
    echo $_SESSION['errUpdate'];
    echo "</div>";
    unset($_SESSION['errUpdate']);
}
echo '
</div>
</form>
</div>
</div>
</div>
</div>';

include('footer.php');
?>
